using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseDoorSound : MonoBehaviour
{
    public MusicController mc;

    public void Sound()
    {          
        GameObject doorsfx = Instantiate(mc.GetSound(9), transform.position, transform.rotation);
        Destroy(doorsfx, 2f);
    }
}

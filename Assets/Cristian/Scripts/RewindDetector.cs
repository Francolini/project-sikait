using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewindDetector : MonoBehaviour
{
    public bool playerAiming, playerRewinding, playerCanRewind;
    public bool npcCanRewind, npcIsRewinding;

    [HideInInspector] public int arrivingNumberPieces, piecesLetDown;
    public int numberOfPieces;
    public Mirrors sunLight;

    // Start is called before the first frame update
    void Start()
    {
        playerAiming = false;
        playerRewinding = false;
        npcIsRewinding = false;

        arrivingNumberPieces = 0;
        piecesLetDown = 0;
    }

    private void Update()
    {
        if(arrivingNumberPieces == numberOfPieces)
        {
            if (sunLight != null && npcCanRewind)
            {
                sunLight.activeLight = false;
                sunLight.spotLight.SetActive(false);
            }

            if (npcCanRewind)
            {
                playerCanRewind = true;
                npcCanRewind = false;
            }
            else
            {
                playerCanRewind = false;
                npcCanRewind = true;
            }

            arrivingNumberPieces = 0;          
        }

        if(piecesLetDown == numberOfPieces)
        {
            playerCanRewind = false;
            npcCanRewind = true;

            piecesLetDown = 0;;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieZone : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) {
        if(other.tag.Equals("Player"))
        {
            other.gameObject.GetComponent<PlayerController>().TakeDamage(150);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastTrigger : MonoBehaviour
{
    
    private void OnTriggerEnter(Collider player)
    {
        if(player.tag.Equals("Player"))
        {
            player.GetComponent<PlayerController>().HealPlayer();
            this.enabled = false;
            Destroy(gameObject, 10f);
        }
    }
}

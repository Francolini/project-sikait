using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarkOrb : MonoBehaviour
{
    private GameObject player;
    [SerializeField] private GameObject darkOrbFlarePrefab;
    private PlayerController pc;

    [SerializeField] private int movementSpeed, rotationSpeed;
    private bool canRotate;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        if (player == null) Destroy(gameObject);

        StartCoroutine("Rotate");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * movementSpeed, Space.Self);

        if (canRotate)
        {
            Vector3 destiny = (new Vector3(player.transform.position.x, player.transform.position.y + Random.Range(.6f, 2.3f), player.transform.position.z) - transform.position).normalized;

            Quaternion toRotate = Quaternion.LookRotation(destiny, Vector3.up);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotate, rotationSpeed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.tag.Equals("SpecialAttackSphere") && !other.gameObject.tag.Equals("Anubis"))
        {
            if (other.gameObject.tag.Equals("Player") && player != null)
            {
                pc = other.gameObject.GetComponent<PlayerController>();
                pc.TakeDamage(Random.Range(4, 9));            
                Destroy(gameObject);
            }

            GameObject darkOrbFlare = Instantiate(darkOrbFlarePrefab, transform.position, transform.rotation);
        }
    }

    private IEnumerator Rotate()
    {
        canRotate = true;

        yield return new WaitForSeconds(4f);

        canRotate = false;
    }
}

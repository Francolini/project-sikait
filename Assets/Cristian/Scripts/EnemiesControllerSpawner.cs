using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesControllerSpawner : MonoBehaviour
{
    public PlayerController pc;
    public GameObject enemiesPrefab;
    public Transform spawnerPoint;

    private GameObject enemieSpawner;

    private void Start()
    {
        SpawnEnemies();     
    }

    public void DestroyEnemies()
    {
        Destroy(enemieSpawner);
    }

    public void SpawnEnemies()
    {
        enemieSpawner = Instantiate(enemiesPrefab, spawnerPoint.position, spawnerPoint.rotation);
    }
}

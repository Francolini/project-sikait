using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beetle : MonoBehaviour
{
    public enum States { Idle, Follow, Hook, Attack, SpecialAttack }
    public States beetleStates;

    public GameObject player, specialAttackPositionGO, followPointGO;

    public float raycastOffset, distanceDetection;
    [HideInInspector] public bool beetleIsAttacking;

    [SerializeField] private float speedMovement, speedRotation;

    private Rigidbody rigi;
    private Vector3 patrolPosition, destinyPoint;
    //private SphereCollider col;
    private PlayerController pc;
    private Collider[] enemies;

    private bool calculatingNewPosition, canMove, pathfinding;
    private int enemyArrayPosition;

    private List<Vector3> pathfindingPositions;

    // Start is called before the first frame update
    void Start()
    {
        beetleStates = States.Idle;

        rigi = GetComponent<Rigidbody>();
        //col = GetComponent<SphereCollider>();
        pc = player.GetComponent<PlayerController>();

        pathfindingPositions = new List<Vector3>();

        canMove = true;
        pathfinding = false;
        beetleIsAttacking = false;

        enemyArrayPosition = 0;

        StartCoroutine("CalculateNewPosition");
    }

    // Update is called once per frame
    void Update()
    {
        switch (beetleStates)
        {
            case States.Idle: Idle(); break;
            case States.Follow: Follow(); break;
            case States.Hook: Hook(); break;
            case States.Attack: Attack(); break;
            case States.SpecialAttack: SpecialAttack(); break;
        }
    }

    private void FixedUpdate()
    {
        if (canMove)
        {
            Move(destinyPoint);
        }

        if (!calculatingNewPosition)
        {
            StartCoroutine("CalculateNewPosition");
        }
    }

    private void Idle()
    {
        speedMovement = 250f;
        speedRotation = 250f;

        if (calculatingNewPosition)
        {
            destinyPoint = patrolPosition;
        }

        if (Vector3.Distance(transform.position, player.transform.position) >= 10f)
        {
            StopCoroutine("CalculateNewPosition");

            calculatingNewPosition = true;
            pathfinding = true;
            //col.isTrigger = true;

            beetleStates = States.Follow;
        }

        if (pc.isAttacking)
        {
            StopCoroutine("CalculateNewPosition");

            calculatingNewPosition = true;
            //col.isTrigger = true;
            // QUITAR canMove = false;

            beetleStates = States.Attack;
        }

        if (pc.isHooking)
        {
            StopCoroutine("CalculateNewPosition");

            calculatingNewPosition = true;
            //col.isTrigger = true;
            canMove = false;

            beetleStates = States.Hook;
        }
    }

    private void Follow()
    {
        speedMovement = 500f;
        speedRotation = 750f;

        destinyPoint = followPointGO.transform.position;

        if (Vector3.Distance(transform.position, followPointGO.transform.position) <= 8f)
        {
            pathfindingPositions = new List<Vector3>();

            calculatingNewPosition = false;
            pathfinding = false;
            //col.isTrigger = false;

            beetleStates = States.Idle;
        }

        if (pc.isAttacking)
        {
            transform.position = pc.beetlePosition;
            pathfindingPositions = new List<Vector3>();

            calculatingNewPosition = false;
            pathfinding = false;
            //col.isTrigger = true;
            //canMove = false;

            beetleStates = States.Attack;
        }

        if (pc.isHooking)
        {
            pathfindingPositions = new List<Vector3>();

            calculatingNewPosition = false;
            pathfinding = false;
            //col.isTrigger = true;
            canMove = false;
            beetleStates = States.Hook;
        }
    }

    private void Hook()
    {
        canMove = false;

        transform.position = pc.beetlePosition;

        if (!pc.isHooking)
        {
            calculatingNewPosition = false;
            //col.isTrigger = false;
            canMove = true;

            beetleStates = States.Idle;
        }
    }

    private void Attack()
    {
        // QUITAR transform.position = pc.beetlePosition;

        // TESTS //

        speedMovement = 3000f;
        speedRotation = 2000f;
        destinyPoint = pc.beetlePosition;

        // TESTS //

        beetleIsAttacking = true;

        if (!pc.isAttacking)
        {
            calculatingNewPosition = false;
            beetleIsAttacking = false;
            //col.isTrigger = false;
            canMove = true;

            beetleStates = States.Idle;
        }
    }

    private void SpecialAttack()
    {
        canMove = true;
        beetleIsAttacking = true;

        speedMovement = 4000;
        //speedRotation = 5000f;
        transform.rotation = Quaternion.LookRotation((destinyPoint - transform.position).normalized, Vector3.up);

        if(enemies.Length - 1 >= enemyArrayPosition)
        {
            if (Vector3.Distance(transform.position, destinyPoint) <= .5f || enemies[enemyArrayPosition] == null)
            {
                canMove = false;
                enemyArrayPosition++;

                if(enemies.Length - 1 >= enemyArrayPosition && enemies[enemyArrayPosition] != null)
                {
                    destinyPoint = enemies[enemyArrayPosition].gameObject.transform.position;                    
                }
            }
        }
        else
        {
            destinyPoint = patrolPosition;
            enemyArrayPosition = 0;
            beetleIsAttacking = false;
            beetleStates = States.Idle;
        }
    }

    private void Move(Vector3 destiny)
    {
        Vector3 destinyDirection = destiny - transform.position;
        destinyDirection.Normalize();

        if (Vector3.Distance(transform.position, destiny) > .05f)
        {
            rigi.velocity = transform.forward * speedMovement * Time.deltaTime;

            if (pathfinding)
            {
                if(Physics.Linecast(transform.position, destiny))
                {
                    pathfindingPositions.Add(followPointGO.transform.position);
                    Vector3 actualPathPosition = pathfindingPositions[0];

                    Quaternion toRotation = Quaternion.LookRotation((actualPathPosition - transform.position).normalized, Vector3.up);
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, speedRotation * Time.deltaTime);

                    if(Vector3.Distance(transform.position, actualPathPosition) <= 2f)
                    {
                        pathfindingPositions.RemoveAt(0);
                    }
                }
                else
                {
                    pathfindingPositions = new List<Vector3>();

                    Quaternion toRotation = Quaternion.LookRotation(destinyDirection, Vector3.up);
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, speedRotation * Time.deltaTime);
                }
            }
            else
            {
                Quaternion toRotation = Quaternion.LookRotation(destinyDirection, Vector3.up);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, speedRotation * Time.deltaTime);
            }
        }
        else
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, new Quaternion(0, transform.rotation.y, 0, transform.rotation.w), (speedRotation - 200f) * Time.deltaTime);
            rigi.velocity = Vector3.zero;
        }
    }

    public void GetEnemiesArray(Collider[] enemiesArray)
    {
        if(enemiesArray.Length != 0)
        {
            transform.position = pc.beetlePosition;
            enemies = enemiesArray;
            destinyPoint = enemies[enemyArrayPosition].gameObject.transform.position;

            StopCoroutine("CalculateNewPosition");

            calculatingNewPosition = false;
            pathfinding = false;

            beetleStates = States.SpecialAttack;
        }
    }

    private IEnumerator CalculateNewPosition()
    {
        patrolPosition = new Vector3(player.transform.position.x + Random.Range(-3f, 3.1f), player.transform.position.y + Random.Range(.5f, 5f), player.transform.position.z + Random.Range(-3f, 3.1f));

        float distance = Vector3.Distance(transform.position, patrolPosition);

        if (Physics.Raycast(transform.position, (patrolPosition - transform.position).normalized, distance + 1f))
        {            
            StopCoroutine("CalculateNewPosition");
        }
        else
        {
            calculatingNewPosition = true;
        }

        yield return new WaitForSeconds(2f);

        calculatingNewPosition = false;
    }
}
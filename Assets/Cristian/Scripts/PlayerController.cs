using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private enum States { Idle, IdleMask, Walk, Run, Sprint, FirstAttack, SecondAttack, ThirdAttack, SpecialAttack, Die, JumpStart, JumpMid, JumpLand, ThrowHook, Hooking, Aim, InteractingWithMirror, ThrowSpecialAttack }
    private States standardState, maskState;

    //[SerializeField]
    private bool isBeingHurted, interacting, specialAttackThrowed, isJumping, shootHook, isSprinting, firstAttackEnabled, firstAttackTriggered, secondAttackEnabled, secondAttackTriggered, thirdAttackEnabled, thirdAttackTriggered, canJumpAgain, attackEnded;

    [SerializeField]
    private float speedMovement, speedRotation, jumpForce, constantJumpForce, gravityForce, aimInputY, specialAttackScale;

    [SerializeField]
    private LayerMask groundLayer, rewindZoneLayer, enemiesLayer;

    private float inputMagnitude, maskWeightValue, hurtWeightValue;
    private int health, healthBG;

    private Rigidbody rigi;
    private Animator playerAnim, pivotBeetleAttackAnim;
    private PlayerInput input;
    private Vector2 inputMovement;
    private Vector3 forward, right, constantJumpForceDirection, groundNormalDirection, moveDirection, hookDestiny;
    private Camera mainCamera;
    private RewindDetector rewindDetector  ;

    [HideInInspector] public GameObject mirror, mirrorBase, mirrorLight;

    public Transform raycastGroundOrigin, hookPoint;
    public GameObject beetleAttackPositionGO, hookPostPrefab, hookOrigin, hook, aimingCamera, specialAttackSphere, radiusCalculator, eyeHole;
    public LineRenderer magicRope;
    public Beetle beetleScript;
    public CinemachineFreeLook playerCam;
    public Slider healthSlider, healthSliderBG, timeSlider;
    public GameMenu gameMenu;
    public EnemiesControllerSpawner enemiesController;

    [HideInInspector] public Vector3 beetlePosition;
    
    public float powerBar;
    public bool dead, isTouchingWall, isAttacking, isInGround, canMove, canInteractMirror, canAttack, dieMenuOpened;

    [HideInInspector] public bool isHooking, isThrowingHook, isAiming, canAim;

    //
    [Range(0,1)]
    public float timescale;

    public MusicController mc;

    void Start()
    {
        moveDirection = Vector3.zero;

        rigi = GetComponent<Rigidbody>();
        input = GetComponent<PlayerInput>();
        playerAnim = GetComponent<Animator>();
        magicRope = GetComponent<LineRenderer>();

        pivotBeetleAttackAnim = GameObject.Find("BeetlePivotAttack").GetComponent<Animator>();
        pivotBeetleAttackAnim.Play("IdleBeetle");

        mainCamera = Camera.main;

        canMove = true;
        specialAttackThrowed = false;
        canAttack = true;
        isJumping = false;
        isSprinting = false;
        firstAttackEnabled = false;
        firstAttackTriggered = false;
        secondAttackEnabled = false;
        secondAttackTriggered = false;
        thirdAttackEnabled = false;
        thirdAttackTriggered = false;
        canJumpAgain = true;
        isTouchingWall = false;
        attackEnded = false;
        isHooking = false;
        isThrowingHook = false;
        shootHook = false;
        isAiming = false;
        canAim = true;
        dead = false;
        isBeingHurted = false;        
        interacting = false; 

        jumpForce = 20f;
        constantJumpForce = 0f;
        maskWeightValue = 0f;
        hurtWeightValue = 1f;
        gravityForce = 50f;
        aimInputY = 0f;
        speedRotation = 1000f;
        specialAttackScale = 0f;

        health = 100;
        healthBG = 100;

        standardState = States.Idle;
        maskState = States.IdleMask;

        constantJumpForceDirection = Vector3.forward;
        groundNormalDirection = Vector3.up;

        playerAnim.SetLayerWeight(1, 0f);
        playerAnim.SetLayerWeight(2, 0f);
    }

    private void Update()
    {       
        inputMovement = input.actions["Move"].ReadValue<Vector2>();
        inputMagnitude = Mathf.Abs(inputMovement.magnitude);

        switch (standardState)
        {
            case States.Idle: Idle(); break;
            case States.Walk: Walk(); break;
            case States.Run: Run(); break;
            case States.Sprint: Sprint(); break;
            case States.JumpStart: JumpStart(); break;
            case States.JumpMid: JumpMid(); break;
            case States.JumpLand: JumpLand(); break;
            case States.ThrowHook: ThrowHook(); break;
            case States.Hooking: Hooking(); break;
            case States.Aim: Aim(); break;
            case States.InteractingWithMirror: InteractingWithMirror(); break;
            case States.Die: Die(); break;
        }

        switch (maskState)
        {
            case States.IdleMask: IdleMask(); break;
            case States.FirstAttack: FirstAttack(); break;
            case States.SecondAttack: SecondAttack(); break;
            case States.ThirdAttack: ThirdAttack(); break;
            case States.SpecialAttack: SpecialAttack(); break;
            case States.ThrowSpecialAttack: ThrowSpecialAttack(); break;
        }

        if (input.actions["Aim"].ReadValue<float>() < .1f)
        {
            isAiming = false;
        }
        else if(canAim)
        {
            isAiming = true;
        }

        if (mirror != null && Vector3.Distance(mirror.transform.position, transform.position) > 5f)
        {
            canInteractMirror = false;
            mirror = null;
            mirrorBase = null;
            mirrorLight = null;
        }

        if(!dead && isBeingHurted && hurtWeightValue > 0)
        {
            hurtWeightValue -= Time.deltaTime * 1.65f;
            playerAnim.SetLayerWeight(2, hurtWeightValue);
            playerAnim.SetTrigger("ToHurt");

            if(hurtWeightValue <= 0)
            {
                playerAnim.ResetTrigger("ToHurt");
                isBeingHurted = false;
                hurtWeightValue = 1f;
            }
        }

        if(dead)
        {
            canMove = false;
            isBeingHurted = false;
            playerAnim.SetLayerWeight(2, 0f);
            standardState = States.Die;
        }
    }

    private void FixedUpdate()
    {
        CalculateForwardAndRightDirection();
        CheckGroundAndSlopes();

        if (canMove)
        {
            Move();
        }
        else
        {
            rigi.velocity = Vector3.zero;
        }

        if (!isJumping)
        {
            rigi.useGravity = false;
            rigi.AddForce(-groundNormalDirection * gravityForce, ForceMode.Force);
        }
        else
        {
            //rigi.useGravity = true;

            if (!isTouchingWall)
            {
                if (!isThrowingHook)
                {
                    rigi.AddForce(constantJumpForceDirection * constantJumpForce, ForceMode.Force);

                    if(!isHooking)
                    {
                        rigi.AddForce(Vector3.down * 20f, ForceMode.Force);
                    }
                    else
                    {
                        rigi.AddForce(Vector3.up * 500f, ForceMode.Force);                        
                    }
                }
                else
                {
                    //rigi.useGravity = true;                    
                    rigi.AddForce(Vector3.up * 20f, ForceMode.Force);
                }
            }
            else
            {
                constantJumpForce = 0;
                rigi.AddForce(Vector3.down * 50f, ForceMode.Force);
            }
        }
    }

    private void Idle()
    {
        canAim = true;
        //canMove = true;
        isSprinting = false;

        speedMovement = 0f;
        gravityForce = 500f;

        playerAnim.SetTrigger("ToIdle");

        if (isAiming)
        {
            playerAnim.ResetTrigger("ToIdle");
            standardState = States.Aim;
        }

        if (inputMagnitude >= .5f)
        {
            playerAnim.ResetTrigger("ToIdle");
            standardState = States.Run;
        }

        if (inputMagnitude >= .1f)
        {
            playerAnim.ResetTrigger("ToIdle");
            standardState = States.Walk;
        }

        if (input.actions["Jump"].triggered && isInGround && !isJumping)
        {
            GameObject jumpSound = Instantiate(mc.GetSound(6), transform.position, transform.rotation);
            Destroy(jumpSound, 1f);
            rigi.velocity = new Vector3(rigi.velocity.x, 0, rigi.velocity.z);
            constantJumpForceDirection = Vector3.zero;
            rigi.AddForce(groundNormalDirection * jumpForce, ForceMode.Impulse);

            isJumping = true;
            canAim = false;

            playerAnim.ResetTrigger("ToIdle");
            playerAnim.SetTrigger("ToJumpStart");
            standardState = States.JumpStart;
        }

        if (!isInGround && rigi.velocity.y < -5f)
        {
            constantJumpForceDirection = Vector3.zero;

            isJumping = true;
            canAim = false;

            playerAnim.ResetTrigger("ToIdle");
            playerAnim.SetTrigger("ToJumpMid");
            standardState = States.JumpMid;
        }

        if (input.actions["Interact"].triggered && !beetleScript.beetleIsAttacking && canInteractMirror)
        {
            mirror.GetComponent<Mirrors>().mirrorCamera.gameObject.SetActive(true);
            playerAnim.ResetTrigger("ToIdle");
            standardState = States.InteractingWithMirror;
        }
    }

    private void Walk()
    {
        //canMove = true;
        canAim = true;
        isSprinting = false;

        speedMovement = 200f;
        gravityForce = 35f;

        playerAnim.SetTrigger("ToWalk");

        if (isAiming)
        {
            playerAnim.ResetTrigger("ToWalk");
            standardState = States.Aim;
        }

        if (inputMagnitude >= .5f)
        {
            playerAnim.ResetTrigger("ToWalk");
            standardState = States.Run;
        }

        if (inputMagnitude < .1f)
        {
            playerAnim.ResetTrigger("ToWalk");
            standardState = States.Idle;
        }

        if (input.actions["Jump"].triggered && isInGround && !isJumping)
        {
            GameObject jumpSound = Instantiate(mc.GetSound(6), transform.position, transform.rotation);
            Destroy(jumpSound, 1f);
            rigi.velocity = new Vector3(rigi.velocity.x, 0, rigi.velocity.z);
            constantJumpForceDirection = moveDirection;
            constantJumpForce = 100f;

            rigi.AddForce(groundNormalDirection * (jumpForce), ForceMode.Impulse);

            isJumping = true;
            canAim = false;

            playerAnim.ResetTrigger("ToWalk");
            playerAnim.SetTrigger("ToJumpStart");
            standardState = States.JumpStart;
        }

        if (!isInGround && rigi.velocity.y < -7.5f)
        {
            isJumping = true;
            canAim = false;

            constantJumpForce = 100f;

            constantJumpForceDirection = moveDirection;

            playerAnim.ResetTrigger("ToWalk");
            playerAnim.SetTrigger("ToJumpMid");
            standardState = States.JumpMid;
        }

        if (input.actions["Interact"].triggered && !beetleScript.beetleIsAttacking && canInteractMirror)
        {
            mirror.GetComponent<Mirrors>().mirrorCamera.gameObject.SetActive(true);
            playerAnim.ResetTrigger("ToWalk");
            standardState = States.InteractingWithMirror;
        }
    }

    private void Run()
    {
        //canMove = true;
        canAim = true;
        isSprinting = false;

        speedMovement = 480f;
        gravityForce = 200f;

        playerAnim.SetTrigger("ToRun");

        if (isAiming)
        {
            playerAnim.ResetTrigger("ToRun");
            standardState = States.Aim;
        }

        if (inputMagnitude < .1f)
        {
            playerAnim.ResetTrigger("ToRun");
            standardState = States.Idle;
        }

        if (inputMagnitude >= .1f && inputMagnitude < .5f)
        {
            playerAnim.ResetTrigger("ToRun");
            standardState = States.Walk;
        }

        if (input.actions["Sprint"].triggered)
        {
            isSprinting = true;
            playerAnim.ResetTrigger("ToWalk");
            playerAnim.ResetTrigger("ToRun");
            standardState = States.Sprint;
        }

        if (input.actions["Jump"].triggered && isInGround && !isJumping)
        {
            GameObject jumpSound = Instantiate(mc.GetSound(6), transform.position, transform.rotation);
            Destroy(jumpSound, 1f);
            rigi.velocity = new Vector3(rigi.velocity.x, 0, rigi.velocity.z);
            constantJumpForceDirection = moveDirection;
            constantJumpForce = 200f;

            rigi.AddForce(groundNormalDirection * (jumpForce), ForceMode.Impulse);

            isJumping = true;
            canAim = false;

            playerAnim.ResetTrigger("ToRun");
            playerAnim.SetTrigger("ToJumpStart");
            standardState = States.JumpStart;
        }

        if (!isInGround && rigi.velocity.y < -7.5f)
        {
            isJumping = true;
            canAim = false;

            constantJumpForce = 200f;

            constantJumpForceDirection = moveDirection;

            playerAnim.ResetTrigger("ToRun");
            playerAnim.SetTrigger("ToJumpMid");
            standardState = States.JumpMid;
        }

        if (input.actions["Interact"].triggered && !beetleScript.beetleIsAttacking && canInteractMirror)
        {
            mirror.GetComponent<Mirrors>().mirrorCamera.gameObject.SetActive(true);
            playerAnim.ResetTrigger("ToRun");
            standardState = States.InteractingWithMirror;
        }
    }

    private void Sprint()
    {
        //canMove = true;
        canAim = true;

        speedMovement = 700f;
        gravityForce = 300f;

        playerAnim.SetTrigger("ToSprint");

        if (isAiming)
        {
            playerAnim.ResetTrigger("ToSprint");
            standardState = States.Aim;
        }

        if (inputMagnitude < .1f)
        {
            playerAnim.ResetTrigger("ToSprint");
            standardState = States.Idle;
        }

        if (inputMagnitude < .5f)
        {
            playerAnim.ResetTrigger("ToSprint");
            standardState = States.Walk;
        }

        if (input.actions["Jump"].triggered && isInGround && !isJumping)
        {
            GameObject jumpSound = Instantiate(mc.GetSound(6), transform.position, transform.rotation);
            Destroy(jumpSound, 1f);

            rigi.velocity = new Vector3(rigi.velocity.x, 0, rigi.velocity.z);
            constantJumpForceDirection = moveDirection;
            constantJumpForce = 300f;

            rigi.AddForce(groundNormalDirection * (jumpForce), ForceMode.Impulse);

            isJumping = true;
            canAim = false;

            playerAnim.ResetTrigger("ToSprint");
            playerAnim.SetTrigger("ToJumpStart");
            standardState = States.JumpStart;
        }

        if (!isInGround && rigi.velocity.y < -10f)
        {
            isJumping = true;
            canAim = false;

            constantJumpForce = 300f;

            constantJumpForceDirection = moveDirection;

            playerAnim.ResetTrigger("ToSprint");
            playerAnim.SetTrigger("ToJumpMid");
            standardState = States.JumpMid;
        }

        if (input.actions["Interact"].triggered && !beetleScript.beetleIsAttacking && canInteractMirror)
        {
            mirror.GetComponent<Mirrors>().mirrorCamera.gameObject.SetActive(true);
            playerAnim.ResetTrigger("ToSprint");
            standardState = States.InteractingWithMirror;
        }
    } 

    private void JumpStart()
    {
        speedMovement = 150f;
        speedRotation = 250;

        canAttack = false;

        rigi.AddForce(Vector3.down * 10, ForceMode.Force);

        if (rigi.velocity.y < -.05f)
        {
            playerAnim.ResetTrigger("ToJumpStart");
            playerAnim.SetTrigger("ToJumpMid");
            standardState = States.JumpMid;
        }

        if(input.actions["Jump"].triggered && canJumpAgain && rigi.velocity.y < 7f && !isTouchingWall && !beetleScript.beetleIsAttacking)
        {
            isHooking = true;
            isThrowingHook = true;
            canMove = false;
            canJumpAgain = false;

            rigi.velocity = Vector3.zero;

            hookDestiny = hookPoint.transform.position;

            beetlePosition = hookDestiny;

            constantJumpForceDirection = transform.forward;

            playerAnim.ResetTrigger("ToJumpStart");
            playerAnim.SetTrigger("ToThrowHook");

            standardState = States.ThrowHook;
        }
    }

    private void JumpMid()
    {
        speedMovement = 150f;
        speedRotation = 250;

        canAttack = false;

        rigi.AddForce(Vector3.down * 10, ForceMode.Force);

        if (isInGround)
        {
            playerAnim.ResetTrigger("ToJumpMid");

            standardState = States.JumpLand;
        }

        if (input.actions["Jump"].triggered && canJumpAgain && !isTouchingWall && !beetleScript.beetleIsAttacking)
        {
            isHooking = true;
            isThrowingHook = true;
            canMove = false;
            canJumpAgain = false;

            rigi.velocity = Vector3.zero;

            hookDestiny = hookPoint.transform.position;

            beetlePosition = hookDestiny;

            constantJumpForceDirection = transform.forward;

            playerAnim.ResetTrigger("ToJumpMid");
            playerAnim.SetTrigger("ToThrowHook");

            standardState = States.ThrowHook;
        }
    }

    private void JumpLand()
    {
        canJumpAgain = true;
        canAttack = true;

        speedRotation = 1000;
        speedMovement = 0;
        constantJumpForce = 0;

        playerAnim.Play("LandJump");

        if (inputMagnitude < .1f)
        {
            standardState = States.Idle;
        }

        if (inputMagnitude >= .1f)
        {
            standardState = States.Walk;
        }

        if (inputMagnitude >= .5f)
        {
            if (isSprinting)
            {
                standardState = States.Sprint;
            }
            else
            {
                standardState = States.Run;
            }
        }

        isJumping = false;

        if (input.actions["Jump"].triggered)
        {
            rigi.AddForce((Vector3.up + Vector3.forward).normalized * jumpForce);

            isJumping = true;

            playerAnim.SetTrigger("ToJumpStart");
            standardState = States.JumpStart;
        }
    }

    private void ThrowHook()
    {
        canMove = false;

        if (isThrowingHook && shootHook)
        {
            constantJumpForce = 0;

            magicRope.enabled = true;

            magicRope.SetPosition(0, hookOrigin.transform.position);
            magicRope.SetPosition(1, hook.transform.position);

            hook.transform.position = Vector3.MoveTowards(hook.transform.position, hookDestiny, 15f * Time.deltaTime);

            if(Vector3.Distance(hook.transform.position, hookDestiny) < .05f)
            {
                HingeJoint hinge = gameObject.AddComponent<HingeJoint>();

                GameObject hookPost = Instantiate(hookPostPrefab, hookDestiny, hookPoint.transform.rotation);
                hinge.connectedBody = hookPost.GetComponent<Rigidbody>();

                isHooking = true;
                isThrowingHook = false;
                shootHook = false;

                playerAnim.ResetTrigger("ToThrowHook");
                playerAnim.SetTrigger("ToHooking");

                standardState = States.Hooking;
            }
        }            
    }

    private void Hooking()
    {
        constantJumpForce = 750f;
        canMove = false;

        magicRope.SetPosition(0, hookOrigin.transform.position);

        if (isInGround)
        {
            canMove = true;

            playerAnim.ResetTrigger("ToHooking");

            standardState = States.JumpLand;
        }

        if (!isHooking)
        {
            canMove = true;

            playerAnim.ResetTrigger("ToHooking");
            playerAnim.SetTrigger("ToJumpMid");

            standardState = States.JumpMid;
        }
    }

    private void Die()
    {
        playerAnim.SetLayerWeight(1, 0);
        playerAnim.SetLayerWeight(2, 0);
        playerAnim.Play("Die");

        if (!dieMenuOpened)
        {
            dieMenuOpened = true;
            StartCoroutine("OpenDieMenu");
        }
    }

    private void Aim()
    {
        eyeHole.SetActive(true);

        canAttack = false;
        canMove = false;
        aimingCamera.SetActive(true);
        
        playerAnim.SetTrigger("ToIdle");

        AimingCameraMovement(aimingCamera, null, .5f);

        RaycastHit hit;
        if(Physics.Raycast(aimingCamera.transform.position, aimingCamera.transform.forward, out hit, 750f, rewindZoneLayer))
        {
            rewindDetector = hit.collider.gameObject.GetComponent<RewindDetector>();
            rewindDetector.playerAiming = true;

            if (input.actions["Rewind"].ReadValue<float>() > .1f)
            {
                rewindDetector.playerRewinding = true;
            }
            else
            {
                rewindDetector.playerRewinding = false;
            }

        } else if(rewindDetector != null)
        {
            rewindDetector.playerAiming = false;
            rewindDetector.playerRewinding = false;
        }

        if (!isAiming)
        {
            if (rewindDetector != null)
            {
                rewindDetector.playerAiming = false;
                rewindDetector.playerRewinding = false;
            }

            canAttack = true;
            standardState = States.Idle;

            aimingCamera.SetActive(false);
            eyeHole.SetActive(false);
        }
    }

    private void InteractingWithMirror()
    {
        canAttack = false;
        canMove = false;
        interacting = true;       

        playerAnim.SetTrigger("ToIdle");

        AimingCameraMovement(mirrorLight, mirrorBase , .4f);

        if (input.actions["Interact"].triggered)
        {
            canAttack = true;
            interacting = false; 
            mirror.GetComponent<Mirrors>().mirrorCamera.gameObject.SetActive(false);
            standardState = States.Idle;
        }
    }

    private void IdleMask()
    {
        if(isThrowingHook || isHooking || isAiming || interacting || dead)
        {
            canMove = false;
        } 
        else
        {
            canMove = true;
        }

        isAttacking = false;
        pivotBeetleAttackAnim.Play("IdleBeetle");
        CheckPowerBar();

        if (maskWeightValue > 0f)
        {
            maskWeightValue -= Time.deltaTime * 3f;
            playerAnim.SetLayerWeight(1, maskWeightValue);
        }

        if (input.actions["Attack"].triggered && canAttack && !beetleScript.beetleIsAttacking)
        {             
            playerAnim.ResetTrigger("ToIdleMask");
            maskState = States.FirstAttack;
        }

        if (input.actions["SpecialAttack"].ReadValue<float>() > 0f && canAttack && !beetleScript.beetleIsAttacking)
        {
            playerAnim.ResetTrigger("ToIdleMask");
            specialAttackSphere.SetActive(true);
            maskState = States.SpecialAttack;
        }
    }

    private void FirstAttack()
    {
        canMove = false;

        playerAnim.SetTrigger("ToFirstAttack");
        beetlePosition = beetleAttackPositionGO.transform.position;
        CheckPowerBar();

        if (maskWeightValue < 1f)
        {
            maskWeightValue += Time.deltaTime * 3f;
            playerAnim.SetLayerWeight(1, maskWeightValue);
        }

        if(input.actions["Attack"].triggered && secondAttackEnabled && canAttack)
        {
            secondAttackTriggered = true;
        }

        if (attackEnded)
        {
            if (secondAttackTriggered)
            {
                secondAttackEnabled = false;
                secondAttackTriggered = false;
                attackEnded = false;
                isAttacking = false;

                playerAnim.ResetTrigger("ToFirstAttack");

                maskState = States.SecondAttack;
            }
            else
            {
                secondAttackEnabled = false;
                attackEnded = false;
                playerAnim.ResetTrigger("ToFirstAttack");
                playerAnim.SetTrigger("ToIdleMask");

                maskState = States.IdleMask;
            }
        } 
    }

    private void SecondAttack()
    {
        playerAnim.SetTrigger("ToSecondAttack");
        beetlePosition = beetleAttackPositionGO.transform.position;
        CheckPowerBar();

        if (input.actions["Attack"].triggered && thirdAttackEnabled && canAttack)
        {
            thirdAttackTriggered = true;
        }

        if (attackEnded)
        {
            if (thirdAttackTriggered)
            {
                thirdAttackEnabled = false;
                thirdAttackTriggered = false;
                attackEnded = false;
                isAttacking = false;

                playerAnim.ResetTrigger("ToSecondAttack");

                maskState = States.ThirdAttack;
            }
            else
            {
                thirdAttackEnabled = false;
                attackEnded = false;

                playerAnim.ResetTrigger("ToSecondAttack");
                playerAnim.SetTrigger("ToIdleMask");

                maskState = States.IdleMask;
            }
        }
    }

    private void ThirdAttack()
    {
        playerAnim.SetTrigger("ToThirdAttack");
        beetlePosition = beetleAttackPositionGO.transform.position;
        CheckPowerBar();

        if (input.actions["Attack"].triggered && firstAttackEnabled && canAttack)
        {
            firstAttackTriggered = true;
        }

        if (attackEnded)
        {
            if (firstAttackTriggered)
            {
                firstAttackEnabled = false;
                firstAttackTriggered = false;
                attackEnded = false;
                isAttacking = false;

                playerAnim.ResetTrigger("ToThirdAttack");
                maskState = States.FirstAttack;
            }
            else
            {
                firstAttackEnabled = false;
                attackEnded = false;

                playerAnim.ResetTrigger("ToThirdAttack");
                playerAnim.SetTrigger("ToIdleMask");
                maskState = States.IdleMask;
            }
        }
    }

    private void SpecialAttack()
    {
        canMove = false;
        
        playerAnim.SetTrigger("ToSpecialAttack");
        beetlePosition = new Vector3(transform.position.x, transform.position.y + 3f, transform.position.z);

        if (maskWeightValue < 1f)
        {
            maskWeightValue += Time.deltaTime * 3f;
            playerAnim.SetLayerWeight(1, maskWeightValue);
        }

        if(powerBar < 10)
        {
            powerBar += Time.deltaTime * 7f;

            playerCam.m_Orbits[0].m_Radius += Time.deltaTime * 3f;
            playerCam.m_Orbits[1].m_Radius += Time.deltaTime * 3f;
            playerCam.m_Orbits[2].m_Radius += Time.deltaTime * 3f;

            specialAttackScale += Time.deltaTime * 90; //500
            specialAttackSphere.transform.localScale = new Vector3(specialAttackScale, specialAttackScale, specialAttackScale) / 10;
        }
        else if(powerBar > 10)
        {
            powerBar = 10;
        }

        timeSlider.value = powerBar;

        if (input.actions["SpecialAttack"].ReadValue<float>() < 1f)
        {   
            if(specialAttackScale >= 55)
            {
                playerAnim.ResetTrigger("ToSpecialAttack");
                playerAnim.SetTrigger("ToThrowSpecialAttack");
                //poner radio collider
                Collider[] enemies = Physics.OverlapSphere(transform.position, Vector3.Distance(transform.position, radiusCalculator.transform.position), enemiesLayer);
                beetleScript.GetEnemiesArray(enemies);

                specialAttackScale = .1f;
                specialAttackSphere.transform.localScale = new Vector3(specialAttackScale, specialAttackScale, specialAttackScale);
                specialAttackSphere.SetActive(false);

                maskState = States.ThrowSpecialAttack;

                StartCoroutine("ThrowSpecialAttackTime");
            }
            else
            {
                playerAnim.ResetTrigger("ToSpecialAttack");
                playerAnim.SetTrigger("ToIdleMask"); 

                specialAttackScale = .1f;
                specialAttackSphere.transform.localScale = new Vector3(specialAttackScale, specialAttackScale, specialAttackScale);
                specialAttackSphere.SetActive(false);

                maskState = States.IdleMask;               
            }
        }
        //     playerAnim.ResetTrigger("ToSpecialAttack");
        //     playerAnim.SetTrigger("ToIdleMask");
        //     //poner radio collider
        //     Collider[] enemies = Physics.OverlapSphere(transform.position, Vector3.Distance(transform.position, radiusCalculator.transform.position), enemiesLayer);
        //     beetleScript.GetEnemiesArray(enemies);

        //     specialAttackScale = .1f;
        //     specialAttackSphere.transform.localScale = new Vector3(specialAttackScale, specialAttackScale, specialAttackScale);
        //     specialAttackSphere.SetActive(false);

        //     maskState = States.IdleMask;
        // }
    }

    private IEnumerator ThrowSpecialAttackTime()
    {
        specialAttackThrowed = false;
        
        yield return new WaitForSeconds(.6f);

        specialAttackThrowed = true;
    }

    private void ThrowSpecialAttack()
    {
        canMove = false;

        if(specialAttackThrowed)
        {
            playerAnim.ResetTrigger("ToThrowSpecialAttack");
            playerAnim.SetTrigger("ToIdleMask");  

            maskState = States.IdleMask;          
        }
    }

    private void CheckPowerBar()
    {
        if (powerBar > 0)
        {
            powerBar -= Time.deltaTime * 3f;

            if (playerCam.m_Orbits[0].m_Radius > 4f)
            {
                playerCam.m_Orbits[0].m_Radius -= Time.deltaTime * 3f;
                playerCam.m_Orbits[1].m_Radius -= Time.deltaTime * 3f;
                playerCam.m_Orbits[2].m_Radius -= Time.deltaTime * 3f;
            }
        }
        else if (powerBar < 0)
        {
            powerBar = 0;
        }

        timeSlider.value = powerBar;
    }

    private void Move()
    {
        if (inputMagnitude >= .1f)
        {
            moveDirection = forward * inputMovement.y + right * inputMovement.x;
            moveDirection.Normalize();

            rigi.velocity = new Vector3(moveDirection.x * speedMovement * Time.deltaTime, rigi.velocity.y, moveDirection.z * speedMovement * Time.deltaTime);

            Quaternion toRotation = Quaternion.LookRotation(moveDirection, Vector3.up);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, speedRotation * Time.deltaTime);
        }
        else
        {
            rigi.velocity = new Vector3(0, rigi.velocity.y, 0);
        }
    }

    private void AimingCameraMovement(GameObject camera, GameObject body, float limit)
    {
        float rightInputX = input.actions["Look"].ReadValue<Vector2>().x;
        float rightInputY = input.actions["Look"].ReadValue<Vector2>().y;

        if (camera.transform.localRotation.x > limit && rightInputY < 0 || camera.transform.localRotation.x < -limit && rightInputY > 0)
        {
            rightInputY = 0;
        }

        if(standardState == States.Aim)
        {
            camera.transform.Rotate(new Vector3(1, 0, 0), -rightInputY * 3f);
            transform.Rotate(new Vector3(0, 1, 0), rightInputX * 3f);
        }
        else
        {
            camera.transform.Rotate(new Vector3(1, 0, 0), -rightInputY * 3f);
            body.transform.Rotate(new Vector3(0, 1, 0), rightInputX * 3f);
        }
    }

    private void CheckGroundAndSlopes()
    {
        RaycastHit hit;
        if (Physics.Raycast(raycastGroundOrigin.position, Vector3.down, out hit, .3f))
        {
            groundNormalDirection = hit.normal;
            groundNormalDirection.Normalize();

            isInGround = true;
        }
        else
        {
            isInGround = false;
        }

        if (isJumping)
        {
            if(Physics.Raycast(transform.position + Vector3.up / 1.5f, constantJumpForceDirection, 1f, groundLayer))
            {
                isTouchingWall = true;
            }
        }
        else
        {
            isTouchingWall = false;
        }
    }

    private void CalculateForwardAndRightDirection()
    {
        forward = mainCamera.transform.forward;
        forward = new Vector3(forward.x, 0, forward.z);

        right = mainCamera.transform.right;
        right = new Vector3(right.x, 0, right.z);

        forward.Normalize();
        right.Normalize();
    }

    private void FirstAttackEnabled()
    {
        firstAttackEnabled = true;
    }

    private void SecondAttackEnabled()
    {
        secondAttackEnabled = true;
    }

    private void ThirdAttackEnabled()
    {
        thirdAttackEnabled = true;
    }

    private void EndAttack()
    {
        attackEnded = true;
    }

    private void FirstBeetleAttack()
    {
        isAttacking = true;
        pivotBeetleAttackAnim.Play("FirstBeetleAttack");
    }

    private void SecondBeetleAttack()
    {
        isAttacking = true;
        pivotBeetleAttackAnim.Play("SecondBeetleAttack");
    }

    private void ThirdBeetleAttack()
    {
        isAttacking = true;
        pivotBeetleAttackAnim.Play("ThirdBeetleAttack");
    }

    private void ShootHook()
    {
        shootHook = true;
        hook.transform.position = hookOrigin.transform.position;
    }

    public void TakeDamage(int dmg)
    {
        if (!dead)
        {
            health -= dmg;

            healthSlider.value = health;
            StopCoroutine("HealthSliderBG");
            StartCoroutine("HealthSliderBG");
            
            if(!isBeingHurted)
            {
                GameObject hurtSound = Instantiate(mc.GetSound(2), transform.position, transform.rotation);
                Destroy(hurtSound, 1f);

                isBeingHurted = true;
                playerAnim.SetLayerWeight(2, hurtWeightValue);
            }

            if (health <= 0)
            {
                health = 0;
                dead = true;
            }
        }
    }

    private IEnumerator HealthSliderBG()
    {
        while (healthBG > health)
        {
            yield return new WaitForSeconds(.05f);
            healthBG--;
            healthSliderBG.value = healthBG;
        }

        yield return null;
    } 

    private IEnumerator OpenDieMenu()
    {
        yield return new WaitForSeconds(2.5f);

        gameMenu.ActiveDieMenu();
    }

    public void HealPlayer()
    {
        health = 100;
        healthBG = 100;

        healthSlider.value = health;
        healthSliderBG.value = healthBG;        
    }

    public void RestartPlayer()
    {
        moveDirection = Vector3.zero;

        rigi = GetComponent<Rigidbody>();
        input = GetComponent<PlayerInput>();
        playerAnim = GetComponent<Animator>();
        magicRope = GetComponent<LineRenderer>();

        pivotBeetleAttackAnim = GameObject.Find("BeetlePivotAttack").GetComponent<Animator>();
        pivotBeetleAttackAnim.Play("IdleBeetle");

        mainCamera = Camera.main;

        canMove = true;
        specialAttackThrowed = false;
        canAttack = true;
        isJumping = false;
        isSprinting = false;
        firstAttackEnabled = false;
        firstAttackTriggered = false;
        secondAttackEnabled = false;
        secondAttackTriggered = false;
        thirdAttackEnabled = false;
        thirdAttackTriggered = false;
        canJumpAgain = true;
        isTouchingWall = false;
        attackEnded = false;
        isHooking = false;
        isThrowingHook = false;
        shootHook = false;
        isAiming = false;
        canAim = true;
        dead = false;
        isBeingHurted = false;        
        interacting = false; 

        jumpForce = 20f;
        constantJumpForce = 0f;
        maskWeightValue = 0f;
        hurtWeightValue = 1f;
        gravityForce = 50f;
        aimInputY = 0f;
        speedRotation = 1000f;
        specialAttackScale = 0f;

        health = 100;
        healthBG = 100;
        powerBar = 0;

        healthSlider.value = health;
        healthSliderBG.value = healthBG;
        timeSlider.value = powerBar;

        standardState = States.Idle;
        maskState = States.IdleMask;

        constantJumpForceDirection = Vector3.forward;
        groundNormalDirection = Vector3.up;

        playerAnim.SetLayerWeight(1, 0f);
        playerAnim.SetLayerWeight(2, 0f);

        playerAnim.ResetTrigger("ToFirstAttack");
        playerAnim.ResetTrigger("ToSecondAttack");
        playerAnim.ResetTrigger("ToThirdAttack");
        playerAnim.ResetTrigger("ToSpecialAttack");
        playerAnim.ResetTrigger("ToDie");
        
        playerAnim.Play("Idle");
        playerAnim.Play("IdleMask");

        beetleScript.gameObject.transform.position = new Vector3(transform.position.x, transform.position.y + 2f, transform.position.z);
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnubisController : MonoBehaviour
{
    public enum States { Idle, Walk, Attack01, Attack02, ChargeSpecialAttack, SpecialAttack01, SpecialAttack02, Resting, Exposed, Rewind, Die, BattleNotStarted }
    public States bossState, auxiliarState;

    private float darkPower, darkPowerLimitDecrease;
    private int health, healthBG, numDarkSpiders, numDarkOrbs;
    private bool thinkingAttack, hasCasted;
    private string typeOfAttack;
    private Slider healthSlider, healthSliderBG, darkPowerSlider;

    private Animator anim;
    private Collider col;
    private Vector3 destiny;
    private GameObject pointToGo;

    [SerializeField] private float movementSpeed, rotationSpeed, minThinkTime, maxThinkTime, decreaseDarkPowerMultiplier;
    [SerializeField] private GameObject darkOrbFlarePrefab;

    public bool beginBattle, isAttacking, isHurtable, isExposed;

    public GameObject darkSpiderPrefab, darkOrbsPrefab, darkOrbsSpawnPoint, hitFXPrefab;
    private GameObject[] placesBossCanGo;
    private GameObject player;
    private RewindDetector detector;
    public Beetle beetle;
    private PuertaFinal pf;

    public GameObject darkZone;
    private MusicController mc;

    // Start is called before the first frame update
    void Start()
    {
        health = 100;
        healthBG = 100;
        darkPower = 100;

        thinkingAttack = false;
        beginBattle = false;
        isAttacking = false;
        hasCasted = false;
        isHurtable = false;

        placesBossCanGo = new GameObject[6];
        
        placesBossCanGo[0] = GameObject.Find("RewindPlace");
        placesBossCanGo[1] = GameObject.Find("ChargeSpecialAttackPoint");
        placesBossCanGo[2] = GameObject.Find("Rest01");
        placesBossCanGo[3] = GameObject.Find("Rest02");
        placesBossCanGo[4] = GameObject.Find("Rest03");
        placesBossCanGo[5] = GameObject.Find("Rest04");

        anim = GetComponent<Animator>();
        detector = GameObject.Find("BossRewindZone").GetComponent<RewindDetector>();
        destiny = Vector3.zero;

        player = GameObject.Find("Player");
        beetle = GameObject.Find("Beetle").GetComponent<Beetle>();
        mc = GameObject.Find("MusicController").GetComponent<MusicController>();

        pointToGo = player;

        bossState = States.BattleNotStarted;

        pf = GameObject.Find("door_anim").GetComponent<PuertaFinal>();
    }

    // Update is called once per frame
    void Update()
    {
        destiny = pointToGo.transform.position;

        switch (bossState)
        {
            case States.Idle: Idle(); break;
            case States.Walk: Walk(); break;
            case States.Attack01: Attack01(); break;
            case States.Attack02: Attack02(); break;
            case States.ChargeSpecialAttack: ChargeSpecialAttack(); break;
            case States.SpecialAttack01: SpecialAttack01(); break;
            case States.SpecialAttack02: SpecialAttack02(); break;
            case States.Resting: Resting(); break;
            case States.Exposed: Exposed(); break;
            case States.Rewind: Rewind(); break;
            case States.Die: Die(); break;
            case States.BattleNotStarted: BattleNotStarted(); break;
        }
   }

    private void Idle()
    {
        anim.SetTrigger("ToIdle");

        if (!thinkingAttack)
        {
            StartCoroutine("ThinkNextAttack");
        }
    }

    private void Walk()
    {
        Rotate();

        if (Vector3.Distance(transform.position, destiny) > 2f)
        {
            anim.SetTrigger("ToWalk");
            Move();
        }
        else
        {
            anim.ResetTrigger("ToWalk");

            if (auxiliarState == States.SpecialAttack01 || auxiliarState == States.SpecialAttack02) 
            { 
                bossState = States.ChargeSpecialAttack; 
                darkPowerLimitDecrease = Random.Range(20f, 36f); 
            }
            else bossState = auxiliarState;
        }
    }

    private void Attack01()
    {
        anim.SetTrigger("ToNormalAttack01");
        isAttacking = true;
    }

    private void Attack02()
    {
        anim.SetTrigger("ToNormalAttack02");
        isAttacking = true;
    }

    private void ChargeSpecialAttack()
    {
        anim.SetTrigger("ToChargeSpecialAttack");

        darkPower -= Time.deltaTime * decreaseDarkPowerMultiplier;
        darkPowerLimitDecrease -= Time.deltaTime * decreaseDarkPowerMultiplier;

        darkPowerSlider.value = darkPower;

        if(darkPower < 0)
        {
            darkPower = 0;
        }

        if(darkPowerLimitDecrease <= 0 || darkPower <= 0)
        {
            anim.ResetTrigger("ToChargeSpecialAttack");

            if (auxiliarState == States.SpecialAttack01)
            {              
                numDarkSpiders = Random.Range(4, 9); 

                GameObject laughSound = Instantiate(mc.GetSound(10), darkOrbsSpawnPoint.transform.position, transform.rotation);
                Destroy(laughSound, 5f);                  
            }              
            else
            {
                numDarkOrbs = Random.Range(5, 11); 

                GameObject laughSound = Instantiate(mc.GetSound(10), darkOrbsSpawnPoint.transform.position, transform.rotation);
                Destroy(laughSound, 5f);
            } 

            bossState = auxiliarState;
        }
    }

    private void SpecialAttack01()
    {
        anim.SetTrigger("ToSpecialAttack01");

        if (!hasCasted && numDarkSpiders > 0)
        {
            StartCoroutine("CastDarkSpider");
        }

        if(numDarkSpiders == 0)
        {
            anim.ResetTrigger("ToSpecialAttack01");

            if (darkPower > 0) bossState = States.Idle;
            else
            {
                int randomRestingPlace = Random.Range(2, 6);
                pointToGo = placesBossCanGo[randomRestingPlace];

                auxiliarState = States.Resting;
                bossState = States.Walk;
            }
        }
    }

    private void SpecialAttack02()
    {
        anim.SetTrigger("ToSpecialAttack02");

        if (!hasCasted && numDarkOrbs > 0)
        {
            StartCoroutine("CastDarkOrb");
        }

        if (numDarkOrbs == 0)
        {
            anim.ResetTrigger("ToSpecialAttack02");

            if (darkPower > 0) bossState = States.Idle;
            else
            {
                int randomRestingPlace = Random.Range(2, 6);
                pointToGo = placesBossCanGo[randomRestingPlace];

                auxiliarState = States.Resting;
                bossState = States.Walk;
            }
        }
    }

    private void Resting()
    {
        darkZone.SetActive(false);

        if (isExposed)
        {
            GameObject painSound = Instantiate(mc.GetSound(12), transform.position, transform.rotation);
            Destroy(painSound, 3f);

            anim.ResetTrigger("ToResting");
            bossState = States.Exposed;
        }
        else
        {
            anim.SetTrigger("ToResting");
        }

        darkPower += Time.deltaTime * 4f;
        darkPowerSlider.value = darkPower;

        if (darkPower >= 100 && detector.npcCanRewind)
        {
            darkZone.SetActive(true);
            anim.ResetTrigger("ToResting");

            pointToGo = placesBossCanGo[0];

            auxiliarState = States.Rewind;
            bossState = States.Walk;
        }else if(darkPower >= 100)
        {
            anim.ResetTrigger("ToResting");
            bossState = States.Idle;
        }
    }

    private void Exposed()
    {
        darkZone.SetActive(false);

        anim.SetTrigger("ToExposed");
        isHurtable = true;

        darkPower += Time.deltaTime * 2f;
        darkPowerSlider.value = darkPower;

        if (darkPower >= 100)
        {
            darkZone.SetActive(true);
            darkPower = 100;
            isHurtable = false;

            anim.ResetTrigger("ToExposed");

            pointToGo = placesBossCanGo[0];

            auxiliarState = States.Rewind;
            bossState = States.Walk;
        }

        if(health <= 0)
        {
            isHurtable = false;
            anim.ResetTrigger("ToExposed");
            bossState = States.Die;
        }
    }

    private void Rewind()
    {
        anim.SetTrigger("ToRewind");

        detector.npcIsRewinding = true;

        if (detector.playerCanRewind)
        {
            isExposed = false;

            detector.npcIsRewinding = false;

            anim.ResetTrigger("ToRewind");
            bossState = States.Idle;
        }
    }

    private void Die()
    {
        anim.SetTrigger("ToDie");
    }

    private IEnumerator ThinkNextAttack()
    {
        thinkingAttack = true;

        int randomAttack = Random.Range(0, 6);

        yield return new WaitForSeconds(Random.Range(minThinkTime, maxThinkTime));

        if(randomAttack >= 0 && randomAttack <= 2)
        {
            pointToGo = player;
            randomAttack = Random.Range(0, 2);

            if (randomAttack == 0) auxiliarState = States.Attack01; 
            else auxiliarState = States.Attack02;
        }
        else
        {
            pointToGo = placesBossCanGo[1];
            randomAttack = Random.Range(0, 5);

            if (randomAttack > 2) auxiliarState = States.SpecialAttack01;
            else auxiliarState = States.SpecialAttack02;
        }

        anim.ResetTrigger("ToIdle");
        bossState = States.Walk;

        thinkingAttack = false;
    }

    private IEnumerator CastDarkSpider()
    {
        hasCasted = true;

        yield return new WaitForSeconds(Random.Range(1.5f, 2f));

        GameObject darkSpider = Instantiate(darkSpiderPrefab, new Vector3(transform.position.x + Random.Range(-10, 11), transform.position.y + 1f, transform.position.z + Random.Range(-10, 11)), transform.rotation);
        numDarkSpiders--;

        hasCasted = false;
    }

    private IEnumerator CastDarkOrb()
    {
        hasCasted = true;

        yield return new WaitForSeconds(Random.Range(.8f, 1.5f));

        GameObject darkOrb = Instantiate(darkOrbsPrefab, darkOrbsSpawnPoint.transform.position, transform.rotation);
        GameObject darkOrbFlare = Instantiate(darkOrbFlarePrefab, darkOrbsSpawnPoint.transform.position, transform.rotation);
        GameObject darkOrbSound = Instantiate(mc.GetSound(1), darkOrbsSpawnPoint.transform.position, transform.rotation);

        
        Destroy(darkOrb, 15f);
        Destroy(darkOrbFlare, 2f);
        Destroy(darkOrbSound, 3f);

        numDarkOrbs--;

        hasCasted = false;
    }

    private void Move()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * movementSpeed, Space.Self);
    }

    private void Rotate()
    {
        Vector3 destinyDirection = (destiny - transform.position).normalized;
        destinyDirection.y = 0;

        Quaternion toRotation = Quaternion.LookRotation(destinyDirection, Vector3.up);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, rotationSpeed * Time.deltaTime);
    }

    public void StopAttacking()
    {
        isAttacking = false;

        anim.ResetTrigger("ToNormalAttack01");
        anim.ResetTrigger("ToNormalAttack02");

        bossState = States.Idle;
    }

    private void BattleNotStarted()
    {
        if (beginBattle)
        {
            anim.ResetTrigger("ToIdle");

            pointToGo = placesBossCanGo[0];

            bossState = States.Walk;
            auxiliarState = States.Rewind;
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if(isHurtable && beetle.beetleIsAttacking && collider.tag.Equals("Beetle"))
        {
            GameObject hitFX = Instantiate(hitFXPrefab, collider.ClosestPoint(transform.position + Vector3.up), transform.rotation);
            Destroy(hitFX, 2f);
            
            TakeDamage();
        }
    }

    private void TakeDamage()
    {
        health -= (int)Random.Range(4f, 9f);
        healthSlider.value = health;

        StopCoroutine("BossHealthSliderBG");
        StartCoroutine("BossHealthSliderBG");
    }

    private IEnumerator BossHealthSliderBG()
    {
        while (healthBG > health)
        {
            yield return new WaitForSeconds(.05f);
            healthBG--;
            healthSliderBG.value = healthBG;
        }

        yield return null;
    } 

    public void EndGame()
    {
        pf.actived = true;
        player.GetComponent<PlayerController>().enabled = false;
    }

    public void GetSliders()
    {        
        healthSlider = GameObject.Find("Boss slider").GetComponent<Slider>();
        healthSliderBG = GameObject.Find("Boss sliderBG").GetComponent<Slider>();
        darkPowerSlider = GameObject.Find("Dark Power slider").GetComponent<Slider>();        
    }

    public void FootStepSound()
    {
        GameObject fsSound = Instantiate(mc.GetSound(8), transform.position, transform.rotation);
        Destroy(fsSound, 1f);
    }
}
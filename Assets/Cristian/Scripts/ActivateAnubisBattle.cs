using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ActivateAnubisBattle : MonoBehaviour
{
    private AnubisController anubisScript;
    public GameObject[] slider;
    public MusicController mc;

    private bool bossActivated = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            anubisScript = GameObject.Find("Anubis").GetComponent<AnubisController>();
            anubisScript.beginBattle = true;

            if(!bossActivated)
            {
                GameObject anubisRoawr = Instantiate(mc.GetSound(0), anubisScript.transform.position, anubisScript.transform.rotation);
                Destroy(anubisRoawr, 4f);

                mc.BossBattle();
                
                bossActivated = true;
            }

            slider[0].SetActive(true);
            slider[1].SetActive(true);
            
            anubisScript.GetSliders();
        }
    }
}

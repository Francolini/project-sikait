using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicController : MonoBehaviour
{
    public AudioSource audioSourceCamera;
    public List<AudioClip> musicClips;
    public List<GameObject> sfxClipsPrefabs;

    public Slider musicSlider, vfxSlider;

    [HideInInspector] public float sfxVolume;
    // Start is called before the first frame update
    void Start()
    {
        audioSourceCamera.loop = true;
        audioSourceCamera.clip = musicClips[0];
        audioSourceCamera.Play();
        sfxVolume = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if(musicSlider != null)
        {
            audioSourceCamera.volume = musicSlider.value;
            sfxVolume = vfxSlider.value;
        }
    }

    public void BossBattle()
    {
        audioSourceCamera.clip = musicClips[1];
        audioSourceCamera.Play();
    }

    public GameObject GetSound (int index)
    {
        return sfxClipsPrefabs[index];
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakePiecesFall : MonoBehaviour
{
    public List<RewindTime> piecesToFall;
    
    private void OnTriggerEnter(Collider other) 
    {
        if(other.tag.Equals("Player"))
        {
            for(int i = 0; i < piecesToFall.Count; i++)
            {
                piecesToFall[i].canStoreData = true;
            }
        } 

        this.GetComponent<Collider>().enabled = false; 
    }

    public void MakePiecesFallAction()
    {        
        for(int i = 0; i < piecesToFall.Count; i++)
        {
            string fallString = "FallStoneBoss0" + (i + 1).ToString();
            piecesToFall[i] = GameObject.Find(fallString).GetComponent<RewindTime>();
            piecesToFall[i].canStoreData = true;
        }        
    }
}

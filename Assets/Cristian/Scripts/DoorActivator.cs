using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorActivator : MonoBehaviour
{
    public Animator doorAnim;

    public void ActivateDoor()
    {
        doorAnim.SetTrigger("ToOpen");
        Destroy(gameObject);
    }
}

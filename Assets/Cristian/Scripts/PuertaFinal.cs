using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaFinal : MonoBehaviour
{
    private Animator myAnim;
    public Animator blackScreenAnim;
    public GameObject cameraGO;
    public bool actived;
    
    // Start is called before the first frame update
    void Start()
    {
        myAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(actived)
        {
            myAnim.Play("FINALDOOR");     
            cameraGO.SetActive(true);       
        }
    }   

    public void EndGame()
    {
        blackScreenAnim.Play("Creditos");
    }
   
}

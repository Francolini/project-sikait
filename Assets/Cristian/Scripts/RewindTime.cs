using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewindTime : MonoBehaviour
{
    private List<Vector3> positions;
    private List<Quaternion> rotations;
    private List<Vector3> callBackPositions;
    private List<Quaternion> callBackRotations;

    private Rigidbody rigidBody;
    public RewindDetector detector;
    public AnubisController npc;

    private float timer;
    private bool maintainSatic, playerIsRewinding, hasLeftTrigger;

    public float backTime;
    public bool temporalRewind, isStatic, canBeRewindedByNPC, canStoreData, npcRewind, hasArrived, hasToEnterResting;

    private GameMenu gm;

    private void Start()
    {
        gm = GameObject.FindGameObjectWithTag("Canvas").GetComponent<GameMenu>();

        positions = new List<Vector3>();
        rotations = new List<Quaternion>();

        if (canBeRewindedByNPC)
        {
            callBackPositions = new List<Vector3>();
            callBackRotations = new List<Quaternion>();

            isStatic = true;
            maintainSatic = false;
            hasLeftTrigger = false;
            detector.npcIsRewinding = false;
            detector.npcCanRewind = true;
            playerIsRewinding = false;
            hasToEnterResting = false;
        }

        rigidBody = GetComponent<Rigidbody>();

        timer = 0;
    }

    private void Update()
    {
        if(gm.inPause) return;

        if (timer <= 0)
        {
            timer = 0;
        }

        if (!canBeRewindedByNPC)
        {
            if (canStoreData)
            {
                if (timer <= backTime && !detector.playerRewinding)
                {
                    StoreData();
                    timer += Time.deltaTime;
                }
                else if (temporalRewind && !detector.playerRewinding)
                {
                    StoreData();
                    DeleteData();
                }

                if (detector.playerRewinding)
                {
                    Rewind();
                }
            }
            else
            {
                rigidBody.isKinematic = true;
            }
        }
        else
        {
            if(npc == null)
            {                
                npc = GameObject.Find("Anubis").GetComponent<AnubisController>();
            }

            if(npc.bossState == AnubisController.States.Resting && hasToEnterResting)
            {
                canStoreData = true;
                hasToEnterResting = false;
            }

            if(npc.bossState == AnubisController.States.Idle)
            {
                hasToEnterResting = true;
                hasLeftTrigger = false;
            }

            if (canStoreData)
            {
                if (timer <= backTime && !maintainSatic)
                {
                    StoreData();
                    timer += Time.deltaTime;
                }
                else
                {
                    maintainSatic = true;
                }

                if (detector.playerCanRewind)
                {
                    if (detector.playerRewinding && !hasLeftTrigger)
                    {
                        Rewind();
                        playerIsRewinding = true;
                        maintainSatic = true;
                        detector.sunLight.activeLight = true;
                        detector.sunLight.spotLight.SetActive(true);

                    } else if (callBackPositions.Count > 0 && playerIsRewinding)
                    {
                        playerIsRewinding = false;
                        hasLeftTrigger = true;
                        maintainSatic = false;

                        rigidBody.isKinematic = false;

                        positions = callBackPositions;
                        rotations = callBackRotations;

                        callBackPositions = new List<Vector3>();
                        callBackRotations = new List<Quaternion>();

                        detector.piecesLetDown++;
                    }
                }

                if(detector.npcIsRewinding && detector.npcCanRewind)
                {
                    Rewind();
                }
            }
        }        
    }

    private void StoreData()
    {
        positions.Insert(0, transform.position);
        rotations.Insert(0, transform.rotation);

        rigidBody.isKinematic = false;
    }

    private void DeleteData()
    {
        positions.RemoveAt(positions.Count - 1);
        rotations.RemoveAt(rotations.Count - 1);
    }

    private void Rewind()
    {
        if (positions.Count == 0 && isStatic)
        {
            if (canBeRewindedByNPC)
            {
                maintainSatic = true;

                detector.arrivingNumberPieces++;

                positions = callBackPositions;
                rotations = callBackRotations;

                callBackPositions = new List<Vector3>();
                callBackRotations = new List<Quaternion>();
            }

            npcRewind = false;
            hasLeftTrigger = false;
            canStoreData = false;
            playerIsRewinding = false;
        }

        if (positions.Count > 0 || rotations.Count > 0)
        {
            timer -= Time.deltaTime;

            transform.position = positions[0];
            transform.rotation = rotations[0];

            positions.RemoveAt(0);
            rotations.RemoveAt(0);

            if (canBeRewindedByNPC)
            {
                callBackPositions.Insert(0, transform.position);
                callBackRotations.Insert(0, transform.rotation);
            }

            rigidBody.isKinematic = true;            
        }
    }

    public void MakePiecesFall()
    {
        if (!gameObject.tag.Equals("Traps"))
        {
            canStoreData = true;
        }
    }
}
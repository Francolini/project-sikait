using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookPost : MonoBehaviour
{
    private float localRotationX, localRotationY, localRotationZ;
    private PlayerController pc;

    // Start is called before the first frame update
    void Awake()
    {
        // localRotationX = 0f;
        // localRotationY = transform.localEulerAngles.y;
        // localRotationZ = transform.localEulerAngles.z;
        localRotationX = transform.localEulerAngles.x;
        localRotationY = transform.localEulerAngles.y;
        localRotationZ = 0f;

        pc = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        localRotationZ += Time.deltaTime * 450f;

        if (pc.isTouchingWall || localRotationZ > 190f || pc.isInGround)
        {
            pc.isHooking = false;
            pc.magicRope.enabled = false;

            HingeJoint hinge = pc.GetComponent<HingeJoint>();
            hinge.breakForce = 0;

            Destroy(hinge);
            Destroy(gameObject);
        }

        if (!pc.isTouchingWall && localRotationZ <= 190f)
        {
            transform.eulerAngles = new Vector3(localRotationX, localRotationY, localRotationZ);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseDoorWalls : MonoBehaviour
{
    public Animator doorAnim;
    
    private void OnTriggerEnter(Collider col)
    {
        if(col.tag.Equals("Player"))
        {
            doorAnim.SetTrigger("Close");
        }
    }
}

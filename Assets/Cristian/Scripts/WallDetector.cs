using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallDetector : MonoBehaviour
{
    [HideInInspector] public bool playerTouchingWall, wallLimit;

    // Start is called before the first frame update
    void Start()
    {
        playerTouchingWall = false;
        wallLimit = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Player") && !wallLimit)
        {
            playerTouchingWall = true;
        }

        if (collision.gameObject.tag.Equals("Traps"))
        {
            wallLimit = true;
            this.enabled = false;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            playerTouchingWall = false;
        }
    }
}

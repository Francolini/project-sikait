using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHit : MonoBehaviour
{
    private PlayerController pc;
    [SerializeField]private int minDamage;
    [SerializeField]private int maxDamage;

    // Start is called before the first frame update
    void Start()
    {
        pc = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            pc.TakeDamage(Random.Range(minDamage, maxDamage + 1));
        }
    }
}

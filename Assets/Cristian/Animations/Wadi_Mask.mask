%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Wadi_Mask
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 1
  - m_Path: Armature/MCH_IK_FORARM.L.001
    m_Weight: 1
  - m_Path: Armature/MCH_IK_FORARM.L.001/MCH_IK_HAND.L
    m_Weight: 1
  - m_Path: Armature/MCH_IK_FORARM.R.001
    m_Weight: 1
  - m_Path: Armature/MCH_IK_FORARM.R.001/MCH_IK_HAND.R
    m_Weight: 1
  - m_Path: Armature/ROOT
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_FOOT.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_FOOT.L/CTRL_FOOT_ROLL.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_FOOT.L/CTRL_POLE_LEG.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_FOOT.L/MCH_FOOT_ROLL_01.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_FOOT.L/MCH_FOOT_ROLL_01.L/MCH_FOOT_ROLL.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_FOOT.L/MCH_FOOT_ROLL_01.L/MCH_FOOT_ROLL.L/MCH_LEG_IK.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_FOOT.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_FOOT.R/CTRL_FOOT_ROLL.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_FOOT.R/CTRL_POLE_LEG.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_FOOT.R/MCH_FOOT_ROLL_01.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_FOOT.R/MCH_FOOT_ROLL_01.R/MCH_FOOT_ROLL.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_FOOT.R/MCH_FOOT_ROLL_01.R/MCH_FOOT_ROLL.R/MCH_LEG_IK.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_CHEST
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_HIPS
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_HIPS/TGT_CUERDA_02
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_HIPS/TGT_CUERDA_04
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_HIPS/TGT_CUERDA_05
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_HIPS/TGT_CUERDA_06
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_HIPS/TGT_HIPS
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_HIPS/TGT_HIPS/MCH_IK_LEG.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_HIPS/TGT_HIPS/MCH_IK_LEG.L/MCH_IK_SHIN.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_HIPS/TGT_HIPS/MCH_IK_LEG.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_HIPS/TGT_HIPS/MCH_IK_LEG.R/MCH_IK_SHIN.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_HIPS/TGT_HIPS/TGT_CUERDA
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_HIPS/TGT_HIPS/TGT_CUERDA/TGT_CUERDA_01
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_HIPS/TGT_HIPS/TGT_LEG.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_HIPS/TGT_HIPS/TGT_LEG.L/TGT_SHIN.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_HIPS/TGT_HIPS/TGT_LEG.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_HIPS/TGT_HIPS/TGT_LEG.R/TGT_SHIN.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_POLE_ARM.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_POLE_ARM.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/DEF_BUFANDA_1
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/DEF_BUFANDA_2
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/DEF_BUFANDA_2/DEF_BUFANDA_2_01
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/DEF_BUFANDA_3
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/DEF_BUFANDA_6
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/DEF_NBUFANDA_4
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/DEF_NBUFANDA_4/DEF_BUFANDA_4_01
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/DEF_NBUFANDA_5
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/MCH_ROT_NECK
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_BUFANDA_1
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_BUFANDA_2
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_BUFANDA_2/TGT_BUFANDA_2_01
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_BUFANDA_3
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_BUFANDA_6
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_CAPA_1
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_CAPA_1/TGT_CAPA_1_2
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_CAPA_2.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_CAPA_2.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_CAPA_3
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_CAPA_3/TGT_CAPA_3_2
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_CAPA_4
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_CAPA_5
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_NBUFANDA_4
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_NBUFANDA_4/TGT_BUFANDA_4_01
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_NBUFANDA_5
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/MCH_ARM-FOLLOW.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/MCH_IK_ARM.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/MCH_IK_ARM.L/MCH_IK_FORARM.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/TGT_ARM.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/TGT_ARM.L/TGT_FORARM.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/TGT_ARM.L/TGT_FORARM.L/TGT_HAND.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/TGT_ARM.L/TGT_FORARM.L/TGT_HAND.L/TGT_PALM_A.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/TGT_ARM.L/TGT_FORARM.L/TGT_HAND.L/TGT_PALM_A.L/TGT_FINGER_A.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/TGT_ARM.L/TGT_FORARM.L/TGT_HAND.L/TGT_PALM_A.L/TGT_FINGER_A.L/TGT_FINGER_A_01.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/TGT_ARM.L/TGT_FORARM.L/TGT_HAND.L/TGT_PALM_B.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/TGT_ARM.L/TGT_FORARM.L/TGT_HAND.L/TGT_PALM_B.L/TGT_FINGER_B.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/TGT_ARM.L/TGT_FORARM.L/TGT_HAND.L/TGT_PALM_B.L/TGT_FINGER_B.L/TGT_FINGER_B_01.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/TGT_ARM.L/TGT_FORARM.L/TGT_HAND.L/TGT_PALM_C.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/TGT_ARM.L/TGT_FORARM.L/TGT_HAND.L/TGT_PALM_C.L/TGT_FINGER_C.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/TGT_ARM.L/TGT_FORARM.L/TGT_HAND.L/TGT_PALM_C.L/TGT_FINGER_C.L/TGT_FINGER_C_01.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/TGT_ARM.L/TGT_FORARM.L/TGT_HAND.L/TGT_PALM_D.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/TGT_ARM.L/TGT_FORARM.L/TGT_HAND.L/TGT_PALM_D.L/TGT_FINGER_D.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/TGT_ARM.L/TGT_FORARM.L/TGT_HAND.L/TGT_PALM_D.L/TGT_FINGER_D.L/TGT_FINGER_D_01.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/TGT_ARM.L/TGT_FORARM.L/TGT_HAND.L/TGT_THUMB.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.L/TGT_ARM.L/TGT_FORARM.L/TGT_HAND.L/TGT_THUMB.L/TGT_THUMB_01.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/MCH_ARM-FOLLOW.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/MCH_IK_ARM.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/MCH_IK_ARM.R/MCH_IK_FORARM.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/TGT_ARM.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/TGT_ARM.R/TGT_FORARM.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/TGT_ARM.R/TGT_FORARM.R/TGT_HAND.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/TGT_ARM.R/TGT_FORARM.R/TGT_HAND.R/TGT_PALM_A.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/TGT_ARM.R/TGT_FORARM.R/TGT_HAND.R/TGT_PALM_A.R/TGT_FINGER_A.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/TGT_ARM.R/TGT_FORARM.R/TGT_HAND.R/TGT_PALM_A.R/TGT_FINGER_A.R/TGT_FINGER_A_01.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/TGT_ARM.R/TGT_FORARM.R/TGT_HAND.R/TGT_PALM_B.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/TGT_ARM.R/TGT_FORARM.R/TGT_HAND.R/TGT_PALM_B.R/TGT_FINGER_B.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/TGT_ARM.R/TGT_FORARM.R/TGT_HAND.R/TGT_PALM_B.R/TGT_FINGER_B.R/TGT_FINGER_B_01.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/TGT_ARM.R/TGT_FORARM.R/TGT_HAND.R/TGT_PALM_C.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/TGT_ARM.R/TGT_FORARM.R/TGT_HAND.R/TGT_PALM_C.R/TGT_FINGER_C.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/TGT_ARM.R/TGT_FORARM.R/TGT_HAND.R/TGT_PALM_C.R/TGT_FINGER_C.R/TGT_FINGER_C_01.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/TGT_ARM.R/TGT_FORARM.R/TGT_HAND.R/TGT_PALM_D.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/TGT_ARM.R/TGT_FORARM.R/TGT_HAND.R/TGT_PALM_D.R/TGT_FINGER_D.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/TGT_ARM.R/TGT_FORARM.R/TGT_HAND.R/TGT_PALM_D.R/TGT_FINGER_D.R/TGT_FINGER_D_01.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/TGT_ARM.R/TGT_FORARM.R/TGT_HAND.R/TGT_THUMB.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/CTRL_TWEAK_SPINE/CTRL_TWEAK_CHEST/TGT_SHOULDER.R/TGT_ARM.R/TGT_FORARM.R/TGT_HAND.R/TGT_THUMB.R/TGT_THUMB_01.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ARM-FOLLOW.L.001
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ARM-FOLLOW.L.001/CTRL_FK_ARM.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ARM-FOLLOW.L.001/CTRL_FK_ARM.L/CTRL_FK_FORARM.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ARM-FOLLOW.L.001/CTRL_FK_ARM.L/CTRL_FK_FORARM.L/CTRL_FK_HAND.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ARM-FOLLOW.R.001
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ARM-FOLLOW.R.001/CTRL_FK_ARM.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ARM-FOLLOW.R.001/CTRL_FK_ARM.R/CTRL_FK_FORARM.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ARM-FOLLOW.R.001/CTRL_FK_ARM.R/CTRL_FK_FORARM.R/CTRL_FK_HAND.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_EYELID
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_eyes
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_CEJA.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_CEJA.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_CEJA_01.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_CEJA_01.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_CORNER_LIP.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_CORNER_LIP.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_EYELID_DOWN.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_EYELID_DOWN.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_EYELID_UP.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_EYELID_UP.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_EYES.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_EYES.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_LIPS_DOWN
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_LIPS_UP
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_MECHON
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_MECHON/TGT_MECHON_01
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_MECHON/TGT_MECHON_01/TGT_MECHON_02
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_MECHON_03
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_MECHON_03/TGT_MECHON_04
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_MECHON_A
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_MECHON_A/TGT_MECHON_A_01
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_MECHON_B
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_MECHON_B/TGT_MECHON_B_01
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_MECHON_C
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_MECHON_C/TGT_MECHON_C_01
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_MID_LIP_DOWN.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_MID_LIP_DOWN.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_MID_LIP_UP.L
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_MID_LIP_UP.R
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_PONYTAIL
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_PONYTAIL/TGT_PONYTAIL_01
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_HEAD/CTRL_HEAD/TGT_PONYTAIL/TGT_PONYTAIL_01/TGT_PONYTAIL_02
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_NECK
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_NECK/CTRL_NECK
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_NECK/CTRL_NECK/MCH_ROT_HEAD
    m_Weight: 1
  - m_Path: Armature/ROOT/CTRL_TORSO/MCH_ROT_INT_NECK/CTRL_NECK/PROP
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_ARM.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_ARM.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CAPA_1
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CAPA_1_2
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CAPA_2.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CAPA_2.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CAPA_3
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CAPA_3_2
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CAPA_4
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CAPA_5
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CEJA.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CEJA.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CEJA_01.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CEJA_01.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CHEST
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CORNER_LIP.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CORNER_LIP.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CUERDA
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CUERDA_01
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CUERDA_02
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CUERDA_04
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CUERDA_05
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_CUERDA_06
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_EYELID_DOWN.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_EYELID_DOWN.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_EYELID_UP.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_EYELID_UP.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_EYES.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_EYES.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FEET.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FEET.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FINGER_A.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FINGER_A.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FINGER_A_01.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FINGER_A_01.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FINGER_B.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FINGER_B.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FINGER_B_01.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FINGER_B_01.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FINGER_C.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FINGER_C.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FINGER_C_01.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FINGER_C_01.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FINGER_D.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FINGER_D.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FINGER_D_01.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FINGER_D_01.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FORARM.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_FORARM.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_HAND.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_HAND.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_HEAD
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_HIPS
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_LEG.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_LEG.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_LIPS_DOWN
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_LIPS_UP
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_MECHON
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_MECHON_01
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_MECHON_02
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_MECHON_03
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_MECHON_04
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_MECHON_A
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_MECHON_A_01
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_MECHON_B
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_MECHON_B_01
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_MECHON_C
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_MECHON_C_01
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_MID_LIP_DOWN.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_MID_LIP_DOWN.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_MID_LIP_UP.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_MID_LIP_UP.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_NECK
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_PALM_A.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_PALM_A.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_PALM_B.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_PALM_B.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_PALM_C.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_PALM_C.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_PALM_D.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_PALM_D.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_PONYTAIL
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_PONYTAIL_01
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_PONYTAIL_02
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_SHIN.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_SHIN.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_SHOULDER.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_SHOULDER.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_SPINE
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_THUMB.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_THUMB.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_THUMB_01.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_THUMB_01.R
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_TOE.L
    m_Weight: 1
  - m_Path: Armature/ROOT/DEF_TOE.R
    m_Weight: 1
  - m_Path: Armature/ROOT/TGT_FEET.L
    m_Weight: 1
  - m_Path: Armature/ROOT/TGT_FEET.L/CTRL_TOE.L
    m_Weight: 1
  - m_Path: Armature/ROOT/TGT_FEET.R
    m_Weight: 1
  - m_Path: Armature/ROOT/TGT_FEET.R/CTRL_TOE.R
    m_Weight: 1
  - m_Path: Brazalete_Low
    m_Weight: 1
  - m_Path: Cuerpo_Low
    m_Weight: 1
  - m_Path: Poncho_Low.001
    m_Weight: 1
  - m_Path: Poncho_Low.002
    m_Weight: 1
  - m_Path: Poncho_Low.003
    m_Weight: 1
  - m_Path: Poncho_Low.004
    m_Weight: 1

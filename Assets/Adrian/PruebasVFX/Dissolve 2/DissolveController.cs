using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;


public class DissolveController : MonoBehaviour
{
    [SerializeField] Animator myAnim;
    [SerializeField] SkinnedMeshRenderer skinnedMeshRenderer1,skinnedMeshRenderer2,skinnedMeshRenderer3,skinnedMeshRenderer4;
    [SerializeField] VisualEffect VFXGraph;
    [SerializeField] float dissolveRate, refreshRate, dieDelay, timeToDestroy;

    private Material[] dissolveMaterial1,dissolveMaterial2,dissolveMaterial3,dissolveMaterial4;
    [SerializeField] bool activeDissolve;
    
            float counter = 0;

                
    // Start is called before the first frame update
    void Start()
    {
        activeDissolve = false;

        dissolveMaterial1 = skinnedMeshRenderer1.materials;

        // if(VFXGraph != null)
        // {
        //     VFXGraph.Stop();
        //     VFXGraph.gameObject.SetActive(false);
        // }
    //    if(skinnedMeshRenderer1 != null)
    //     {
    //         dissolveMaterial1 = skinnedMeshRenderer1.materials;
           
    //     }
    //      if(skinnedMeshRenderer2 != null)
    //     {
    //         dissolveMaterial2 = skinnedMeshRenderer2.materials;
           
    //     }
    //      if(skinnedMeshRenderer3 != null)
    //     {
    //         dissolveMaterial3 = skinnedMeshRenderer3.materials;
           
    //     }
    //      if(skinnedMeshRenderer4 != null)
    //     {
            
    //         dissolveMaterial4 = skinnedMeshRenderer4.materials;
           
    //     }
    }

    // Update is called once per frame
    void Update()
    {
        // if (activeDissolve) StartCoroutine(Dissolve());
        // if (gameObject.activeSelf)
        // {
        //     Mesh m = new Mesh();
        //     skinnedMeshRenderer1.BakeMesh(m);
        //     Vector3[] vertices = m.vertices;
        //     Mesh m2 = new Mesh();
        //     m2.vertices = vertices;

        //     VFXGraph.SetMesh("Mesh", m2);
        // }
        if (activeDissolve)
        {
            counter += dissolveRate;

            //for (int i = 0; i < dissolveMaterial1.Length; i++)
            //{
                dissolveMaterial1[0].SetFloat("DissolveAmount_", counter);
            //}
            /* for (int i = 0; i < dissolveMaterial2.Length; i++)
            {
                dissolveMaterial2[i].SetFloat("DissolveAmount_", counter);
            }
             for (int i = 0; i < dissolveMaterial3.Length; i++)
            {
                dissolveMaterial3[i].SetFloat("DissolveAmount_", counter);
            }
             for (int i = 0; i < dissolveMaterial4.Length; i++)
            {
                dissolveMaterial4[i].SetFloat("DissolveAmount_", counter);
            }*/
            
        }

    }
    void ActiveDissolve()
    {
        activeDissolve = true;
    }
    // IEnumerator Dissolve()
    // {
              
    //     yield return new WaitForSeconds(dieDelay);
    //     if(VFXGraph != null)
    //     {
    //         VFXGraph.gameObject.SetActive(true);
    //         VFXGraph.Play();
    //     }
    //   /*  if (dissolveMaterial.Length > 0)
    //     {
    //         while(dissolveMaterial[0].GetFloat("DissolveAmount_") < 1)
    //         {

    //         }
    //     }*/
    //   //  Destroy(gameObject, timeToDestroy);
    // }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallsTrap : MonoBehaviour
{
    public bool activated, playerDetected;

    public GameObject wallR, wallL, player;
    public Animator doorAnimator;

    [SerializeField]
    private float speed;

    private WallDetector wallLDet, wallRDet;
    private RewindTime wallLRewind, wallRRewind;
    private RewindDetector rewindDet;

    private PlayerController pc;

    private Vector3 initialWall01Pos, initialWall02Pos;

    public Animator door;
   
    void Start()
    {
        pc = GameObject.Find("Player").GetComponent<PlayerController>();
        activated = false;
        playerDetected = false;

        wallLDet = wallL.GetComponent<WallDetector>();
        wallRDet = wallR.GetComponent<WallDetector>();
        wallLRewind = wallL.GetComponent<RewindTime>();
        wallRRewind = wallR.GetComponent<RewindTime>();

        rewindDet = GetComponent<RewindDetector>();

        initialWall01Pos = wallLDet.transform.position;
        initialWall02Pos = wallRDet.transform.position;
    }

   
    void Update()
    {
        if(pc.dead)
        {
            wallLDet.transform.position = initialWall01Pos;
            wallRDet.transform.position = initialWall02Pos;
            
            activated = false;
            playerDetected = false;
            wallLDet.wallLimit = false;

            door.Play("Open");

            return;
        }

        if (!wallLDet.wallLimit)
        {
            if (rewindDet.playerRewinding)
            {
                activated = false;
            }
            else if (playerDetected)
            {
                activated = true;
            }

            if (activated)
            {
                wallL.transform.Translate(-wallL.transform.right * -speed * Time.deltaTime);
                wallR.transform.Translate(wallL.transform.right * -speed * Time.deltaTime);
            }
        }
        else
        {
            wallL.GetComponent<Rigidbody>().isKinematic = true;
            wallR.GetComponent<Rigidbody>().isKinematic = true;

            rewindDet.enabled = false;

            wallLRewind.enabled = false;
            wallRRewind.enabled = false;

            this.enabled = false;
        }
        
        if(wallLDet.playerTouchingWall && wallRDet.playerTouchingWall)
        {
            wallLDet.wallLimit = true;
            player.GetComponent<PlayerController>().TakeDamage(120);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag.Equals("Player"))
        {
            if (!playerDetected)
            {
                doorAnimator.Play("Close 1");

                wallLRewind.backTime = 10f;
                wallRRewind.backTime = 10f;

                wallLRewind.canStoreData = true;
                wallRRewind.canStoreData = true;

                playerDetected = true;
            }
        }        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ParticleSystemJobs;
public class GuardianController : MonoBehaviour
{
    private enum GuardianStates {Statue,Idle, Walk, FirstAttack, SecondAttack,SpecialAttack, Death}
    [SerializeField] GuardianStates guardianState;

    [SerializeField]
    float rangeAttack, rangeVision, speedMovement, speedRotation, distancePlayer, guardianHealth, timeSpecialAttack, timerSpecialAttack, hurtWeightValue;

    [SerializeField]
    bool canRun, hasDied, canRotate, decided, isAttacking, hasAttacked, hasBeenSeen, firstDie,specialAttacking, auxSpecialAttack, wakeUp, isBeingHurted, dead, auxDeathCounter;
    public bool canWakeUp;
    [SerializeField]
    GameObject player, golemPref, stonesPrefab, stonesSecondAttack;
    public GameObject hitFXPrefab;       

    private Collider myCol;
    private Rigidbody myRigi;
  
   [SerializeField] GameObject templeDoor;

    private Animator myanim;
    private Vector3 destiny;

    private MusicController mc;

    void Start()
    {
        mc = GameObject.Find("MusicController").GetComponent<MusicController>();
        canWakeUp = false;
        player = GameObject.Find("Player");    
        myanim = GetComponent<Animator>();   
      
        decided = true;
        hasBeenSeen = false;
        isAttacking = false;        
        destiny = player.transform.position;
        auxSpecialAttack = true;
        hasDied = false;
        hurtWeightValue = .4f;  
        dead = false;         
        myanim.SetLayerWeight(1, 0);
        templeDoor = GameObject.Find("TriggerPuerta_Templo");

        myCol = GetComponent<CapsuleCollider>();
        myRigi = GetComponent<Rigidbody>();
    }
  
    void Update()
    {        
        switch (guardianState)
        {
            case GuardianStates.Statue:
                Statue();
                break;
            case GuardianStates.Idle:
                Think();
                break;
            case GuardianStates.Walk:
                Walk();
                break;
            case GuardianStates.FirstAttack:
                firstAttack();
                break;
            case GuardianStates.SecondAttack:
                secondAttack();
                break;
            case GuardianStates.SpecialAttack:
                specialAttack();              
                break;
            case GuardianStates.Death:
                Die();
                break;
        }

        if (canRun)
        {           
            Run();
        }

        if (canRotate) Rotate();

        distancePlayer = Vector3.Distance(transform.position, player.transform.position);

        if(wakeUp) myanim.speed = 1;
        else myanim.speed = 0;

        if (guardianHealth <= 0f)
        {
            guardianState = GuardianStates.Death;
        }

        if(!dead && isBeingHurted && hurtWeightValue > 0)
        {
            hurtWeightValue -= Time.deltaTime * .75f;
            myanim.SetLayerWeight(1, hurtWeightValue);

            if(hurtWeightValue <= 0)
            {
                isBeingHurted = false;
                hurtWeightValue = .4f;
            }
        }
        if (player.GetComponent<PlayerController>().dead)
        {
            hasBeenSeen = false;
            guardianState = GuardianStates.Statue;
            myanim.Play("Wake Up");
        }
    }

    void Run()
    {       
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(player.transform.position.x, 0, player.transform.position.z), speedMovement * Time.deltaTime);
    }
    void Rotate()
    {
        destiny = player.transform.position;
        Quaternion lookDirection = Quaternion.LookRotation(destiny - transform.position);
        lookDirection.x = 0;
        lookDirection.z = 0;
        transform.rotation = Quaternion.Slerp(transform.rotation, lookDirection, speedRotation * Time.deltaTime);
    }
    void Statue()
    {
        myanim.speed = 0;  
       myRigi.isKinematic = true;
       myCol.enabled = false;
        
        if(distancePlayer < rangeVision && canWakeUp){
            wakeUp = true;
            myRigi.isKinematic = false;
            myCol.enabled = true;
            guardianState = GuardianStates.Idle;            
        }
        canRotate = false;
        canRun = false;        
    }
    void Think()
    {
        hasAttacked = false;
        StartCoroutine(ResetTriggers());
        myanim.speed = 1;               
       
        canRotate = true;
        canRun = false;

        if (distancePlayer < rangeVision)
        {            
            hasBeenSeen = true;
        }
       
        if(hasBeenSeen && !isAttacking && decided && distancePlayer<rangeAttack)
        {
            decided = true;
            StartCoroutine(DecideAttack());
        }   
        if(distancePlayer > rangeAttack && hasBeenSeen)
        {
            guardianState = GuardianStates.Walk;
        }        
    }
    IEnumerator DecideAttack()
    {
        decided = false;
        float timeThinking = Random.Range(1f, 2f);
        int typeAttack = Random.Range(1, 4);

        yield return new WaitForSeconds(timeThinking);

        switch (typeAttack)
        {
            case 1:
                
                destiny = player.transform.position;
                guardianState = GuardianStates.FirstAttack;

                break;
            case 2:
                destiny = player.transform.position;
                guardianState = GuardianStates.SecondAttack;
                break;
            case 3:
                if (!firstDie)
                {
                destiny = player.transform.position;
                guardianState = GuardianStates.SpecialAttack;
                }
                break;
        }

        decided = true;
    }
    void Walk()
    {
      
        myanim.SetTrigger("Walk");
        destiny = player.transform.position;
        decided = false;
        if (!hasBeenSeen)
        {
            guardianState = GuardianStates.Idle;
        }
        canRotate = true;
        canRun = true;

        if(distancePlayer < rangeAttack)
        {
            decided = true;
            guardianState = GuardianStates.Idle;
        }
    }
    void firstAttack()
    {        
        canRun = false;
      
       myanim.SetTrigger("FirstAttack");
        isAttacking = true;
       
        if (hasAttacked)
        {
            decided = true;
            guardianState = GuardianStates.Idle;
        }
    }
    void secondAttack()
    {
        canRun = false;        
      
        myanim.SetTrigger("SecondAttack");
        isAttacking = true;
      
        if (hasAttacked)
        {
            decided = true;
            guardianState = GuardianStates.Idle;
        }
    }
    void specialAttack()
    {
        myanim.SetTrigger("SpecialAttack");
        canRotate = true;

        timerSpecialAttack +=Time.deltaTime;

        if (auxSpecialAttack)
        {
            timerSpecialAttack = 0;
            auxSpecialAttack = false;

            GameObject stones = Instantiate(stonesPrefab,new Vector3(transform.position.x, transform.position.y + 2.8f, transform.position.z), stonesPrefab.transform.rotation);
            stones.transform.parent = transform;

            Destroy(stones, 5f);
        } 
        
        if (distancePlayer < 4f)
        {
            canRun = false;
        }
        else canRun = true;
              
        if (timerSpecialAttack >= 4f)
        {
            decided = true;
           
            specialAttacking = false;
            
            auxSpecialAttack = true;
            guardianState = GuardianStates.Idle;
        }      
    }
   
    void Die()
    {
        transform.position = new Vector3(transform.position.x, -1, transform.position.z);
        myanim.SetTrigger("Die");
        canRotate = false;
        canRun = false;
        dead = true;
        StopAllCoroutines();

        if(!hasDied)
        {
            hasDied = true;
            counterDeathPlus();
        }
        
        if (!firstDie)
        {                     
           InvokeRepeating("invokeMini",2f,0f);
           firstDie = true;
           guardianHealth = guardianHealth + 2f;                    
        }
        else
        {                                  
            Destroy(this.gameObject, 3f);        
        }
    }
    
    void counterDeathPlus() 
    {
       
        templeDoor.GetComponent<TempleDoor>().numberGuardianDead++;
    }
    void activateVFXSecondAttack()
    {
        GameObject stonesFloor = Instantiate(stonesSecondAttack,transform.position + transform.forward * 5, stonesSecondAttack.transform.rotation);
        Destroy(stonesFloor, 4f);
    }
    void desactivateVFXSecondAttack()
    {
       // VFXGO.SetActive(false);
    }
    void invokeMini()
    {
       
        Instantiate(golemPref,new Vector3(transform.position.x + Random.Range(-3,-1),transform.position.y, transform.position.z + Random.Range(-4,-1)), transform.rotation);
        Instantiate(golemPref, new Vector3(transform.position.x + Random.Range(1, 4), transform.position.y, transform.position.z + Random.Range(1, 4)), transform.rotation);
        firstDie = true;       
    }
    public void hasAttackedTrue()
    {            
        isAttacking = false;
        hasAttacked = true;

    }
    IEnumerator ResetTriggers()
    {
        isAttacking = false;
        yield return null;

        myanim.ResetTrigger("SpecialAttack");
        myanim.ResetTrigger("SecondAttack");      
        myanim.ResetTrigger("FirstAttack");

    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Beetle")
        {
            if (other.gameObject.GetComponent<Beetle>().beetleIsAttacking)
            {
                guardianHealth--;   
                GameObject hitFX = Instantiate(hitFXPrefab, other.ClosestPoint(transform.position + Vector3.up), transform.rotation);
                Destroy(hitFX, 2f);        
            
                if(!isBeingHurted)
                {
                    isBeingHurted = true;
                    myanim.SetLayerWeight(1, hurtWeightValue);
                }
                //guardianState = GuardianStates.Hurt;
            } 
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(specialAttacking && collision.gameObject.tag == "Player")
        {          
            // restar vida al player;         
        }
    }

    public void AttackSound()
    {
        GameObject attackSound = Instantiate(mc.GetSound(4), transform.position, transform.rotation);
        Destroy(attackSound, 1f);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempleDoor : MonoBehaviour
{
    public int numberGuardianDead;
    [SerializeField] int numGuardianNeeds;
    [SerializeField] Animator myanim, doorAnim;

public GuardianController guardianScript1, guardianScript2;

    [SerializeField] Camera camPlayer, camTemple;
    // Start is called before the first frame update
    void Start()
    {

        camPlayer.enabled = true;
        camTemple.enabled = false;
        numberGuardianDead = 0; 
        
    }

    // Update is called once per frame
    void Update()
    {
        if(numberGuardianDead >= numGuardianNeeds)
        {
            StartCoroutine(OpenDoor());
        }
    }
    
    IEnumerator OpenDoor()
    {
        numberGuardianDead = 0;
        myanim.Play("FadeIn"); 
        yield return new WaitForSeconds(2.5f);
        
       // canPlayerMove = false;
        camPlayer.enabled = false;
        camTemple.enabled = true;
        
        myanim.Play("FadeOut");
        doorAnim.Play("Open");
        yield return new WaitForSeconds(4f);

            // abrir la puerta

        myanim.Play("FadeIn");
        yield return new WaitForSeconds(2f);
         camPlayer.enabled = true;
        camTemple.enabled = false;
        myanim.Play("FadeOut");
         yield return new WaitForSeconds(2f);
       
        //canPlayerMove = true;
       
    }
    void OnTriggerEnter(Collider col)
    {
     if(col.gameObject.tag == "Player")
     {
      guardianScript1.canWakeUp = true;
      guardianScript2.canWakeUp = true;
     }
    }
    
}

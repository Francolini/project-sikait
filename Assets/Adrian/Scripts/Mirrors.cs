using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Mirrors : MonoBehaviour
{
    [SerializeField]
    public GameObject foco, mirrorBase, spotLight;

    public bool activeLight;
    private PlayerController pc;
    public GameObject player;
    public CinemachineVirtualCamera mirrorCamera;
    public LayerMask mirrorInteractuableLayer;

    [SerializeField]
     Mirrors mirrors;

    [SerializeField]
    float distLight;

    private void Start() {
        player = GameObject.Find("Player");
        pc = player.GetComponent<PlayerController>();
    }
        
    private void Update()
    {
        if(Vector3.Distance(pc.gameObject.transform.position, transform.position) <= 5f)
        {
            pc.canInteractMirror = true;
            pc.mirror = gameObject;
            pc.mirrorBase = mirrorBase;
            pc.mirrorLight = foco;
        }

        if (activeLight)
        {
            spotLight.SetActive(true);
           
            RaycastHit info;
            if (Physics.Raycast(foco.transform.position, foco.transform.forward, out info, distLight, mirrorInteractuableLayer))
            {
                if (info.collider.tag == "HorusEye")
                {
                    info.collider.gameObject.GetComponent<DoorActivator>().ActivateDoor();
                }

                if (info.collider.tag == "Espejo" )
                {
                    mirrors = info.collider.GetComponent<Mirrors>();
                    mirrors.spotLight.SetActive(true);
                    mirrors.activeLight = true;
                }
                
                if(info.collider.tag == "Anubis")
                {
                    info.collider.gameObject.GetComponent<AnubisController>().isExposed = true;
                }
            }
            else
            {
                if(mirrors != null)
                {
                    mirrors.activeLight = false;
                    mirrors.spotLight.SetActive(false);
                    mirrors = null;
                }
            }

        }
        else
        {
            if (mirrors != null)
            {
                mirrors.activeLight = false;
                mirrors.spotLight.SetActive(false);
                mirrors = null;
            }
        }
    }   
}

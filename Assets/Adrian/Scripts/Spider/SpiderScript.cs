using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;


public class SpiderScript : MonoBehaviour
{
    
    [SerializeField]
    GameObject player, projectil, headSpider, parentSpider, pointPatrol1, pointPatrol2;

    public GameObject pointToReturn;
    public Collider colAttack;
    private RigBuilder RigB;
    
    

    [Header("Patitas")]
    [SerializeField] SpiderController L1Leg;
    [SerializeField] SpiderController L2Leg;
    [SerializeField] SpiderController L3Leg;
    [SerializeField] SpiderController L4Leg;

    [SerializeField] SpiderController R1Leg;
    [SerializeField] SpiderController R2Leg;
    [SerializeField] SpiderController R3Leg;
    [SerializeField] SpiderController R4Leg;

    [Header("Parametros")]

    [SerializeField]
    float speedMovement, rangeAttack, spiderHealth, speedRotation,speedAttack, timer, timerToAttack;
    [SerializeField]
    float distancePlayer, distancePointPatrol;

    [SerializeField]
    bool hurt, decided, canMove, canRotate, followingPlayer, hasAttacked, hasBeenSeen, turnPatrol, isAttacking;
    public bool toDie;
    public GameObject hitFXPrefab;

    enum SpiderStates { Run, Think, Hurt, Death, Attack}
    [SerializeField] SpiderStates spiderState;

    private Rigidbody myRigi;
    private Animator myAnim;
    private Vector3 destiny;

    private int typeAttack;

    private MusicController mc;

   Vector3 pointToReturnAfterAttack;

    void Awake()
    {
        StartCoroutine(LegUpdateCoroutine());
    }
    void Start()
    {
        RigB = GetComponent<RigBuilder>();
        turnPatrol = false;
        player = GameObject.FindGameObjectWithTag("Player");
        spiderState = SpiderStates.Run;
        myRigi = GetComponent<Rigidbody>();
        myAnim = GetComponent<Animator>();
        canRotate = true;
        followingPlayer = false;
        canMove = true;
        hasBeenSeen = false;
        decided = true;
        isAttacking = false;

        destiny = pointPatrol1.transform.position;       
        mc = GameObject.Find("MusicController").GetComponent<MusicController>();
    }
    void FixedUpdate()
    {
        if (canMove) Move(destiny);
        if (canRotate) Rotate();       
    }
   
    void Update()
    {   
        if(player != null)
        {
            distancePlayer = Vector3.Distance(player.transform.position, transform.position);
        }

        timer += Time.deltaTime;

        switch (spiderState)
        {
            case SpiderStates.Think:
                Thinking();
                break;
            case SpiderStates.Hurt:
                Hurt();                
                break;
            case SpiderStates.Death:
                Die();
                break;
            case SpiderStates.Run:
                Run();
                break;
            case SpiderStates.Attack:
                MeleAttack();
                break;
        }

        if (spiderHealth <= 0)
        {
            StopCoroutine(LegUpdateCoroutine()); 
            spiderState = SpiderStates.Death;
        }   
    } 

    void Hurt()
    {
        myAnim.SetTrigger("Hurt");
    }

    public void FinishHurt()
    {
        spiderState = SpiderStates.Think;
    }

    void Run()
    {
        myAnim.SetTrigger("Walk");
        canMove = true;
        canRotate = true;

        if (!hasBeenSeen)
        {
            if (turnPatrol) destiny = pointPatrol1.transform.position;
            else destiny = pointPatrol2.transform.position;
            distancePointPatrol = Vector3.Distance(transform.position, destiny);

            if (distancePointPatrol < 2f)
            {
                if (turnPatrol) turnPatrol = false;
                else turnPatrol = true;
            }

            followingPlayer = false;
            speedMovement = 1f;
        }

        if (hasBeenSeen && !isAttacking)
        {
            spiderState = SpiderStates.Think;
           
            destiny = player.transform.position;
            followingPlayer = true;
            speedMovement = 5f;        
        }
             

        if (followingPlayer && !hasAttacked)
        {
            destiny = player.transform.position;
        }

        if(hasAttacked)
        {
            destiny = pointToReturnAfterAttack;     

            if (Vector3.Distance(transform.position, destiny) < 2f)
            {
                hasBeenSeen = true;
                isAttacking = false;
                followingPlayer = true;
                hasAttacked = false;
                destiny = player.transform.position;
                spiderState = SpiderStates.Think;
            }           
        }

        if(distancePlayer >= rangeAttack && hasBeenSeen && !isAttacking)
        {
            destiny = player.transform.position;
            hasAttacked = false;
        }

        if (distancePlayer <= rangeAttack && !isAttacking/* && !hasAttacked*/)
        {
            if(player!= null) destiny = player.transform.position;
            //spiderState = SpiderStates.Think;
           
            //decided = true;
            hasBeenSeen = true;
        }
       

    }
    void Die()
    {
        GetComponent<Collider>().enabled = false;
        RigB.enabled = false;
        myRigi.isKinematic = true;
        toDie = true;
        canMove= false;
        canRotate = false;
        myAnim.SetTrigger("Dead");
        StopCoroutine(LegUpdateCoroutine());
       
    }
       
    void Move(Vector3 target)
    { 
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(target.x, transform.position.y, target.z), speedMovement * Time.deltaTime);
    }
    void Rotate()
    {
        Quaternion lookDirection = Quaternion.LookRotation(destiny - transform.position);
        lookDirection.x = 0;
        lookDirection.z = 0;
        transform.rotation = Quaternion.Slerp(transform.rotation, lookDirection, speedRotation * Time.deltaTime);
    }
    void hasAttackedTrue()
    {
        hasAttacked = true;
        colAttack.enabled = false;
    }
    void Thinking()
    {
        destiny = player.transform.position;
        canMove = false;
        canRotate = true;       

        
        if (decided)
        {
            StartCoroutine(DecideAttack());
        }   
        
    }
    IEnumerator DecideAttack()
    {      
        canMove = false;
        decided = false;

        typeAttack = Random.Range(1, 3);
        timerToAttack = Random.Range(1f, 3f);

        yield return new WaitForSeconds(timerToAttack);
        
        switch (typeAttack)
        {
            case 1:
                //pointToReturnAfterAttack = transform.position;
                destiny = player.transform.position;
                followingPlayer = false;
                spiderState = SpiderStates.Attack;            

                break;
            case 2:

                DistanceAttack();

                break;
        }

        decided = true;       
    }  
    void MeleAttack()
    {
        isAttacking = true;        
        canMove = true;     
        speedMovement = speedAttack;          
               
        if (Vector3.Distance(transform.position, destiny) < 4f)
        {
            GameObject biteSound = Instantiate(mc.GetSound(11), transform.position, transform.rotation);
            Destroy(biteSound, 1.5f);

            canMove = false;
            myAnim.SetTrigger("Bite");
           
            pointToReturnAfterAttack = new Vector3(player.transform.position.x + Random.Range(-4, 5), transform.position.y, player.transform.position.z + Random.Range(-4, 5));
            spiderState = SpiderStates.Run;
                  
        }       
    }  
    void DistanceAttack()
    {
        decided = false;
        myAnim.SetTrigger("RangeAttack");
      

        decided = true;
    }  
    void SpawnMisil()
    {
        Instantiate(projectil, new Vector3(headSpider.transform.position.x, headSpider.transform.position.y +2.5f , headSpider.transform.position.z), Quaternion.identity);
    }
    void Destroy()
    {
        Destroy(parentSpider, 3f);
    }
    
    IEnumerator LegUpdateCoroutine()
    {
        while (true)
        {
            do
            {
                L1Leg.TryMove();
                R2Leg.TryMove();
                R3Leg.TryMove();
                L4Leg.TryMove();
                yield return null;

            } while (L1Leg.moving || R2Leg.moving );

        
            do
            {
            L2Leg.TryMove();
            R1Leg.TryMove();
            R4Leg.TryMove();
            L3Leg.TryMove();
            yield return null;
            } while (L2Leg.moving || R1Leg.moving);
        }
    }   
       
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Beetle" && col.gameObject.GetComponent<Beetle>().beetleIsAttacking)
        {
            if (col.gameObject.GetComponent<Beetle>().beetleIsAttacking)
            {
                GameObject hitFX = Instantiate(hitFXPrefab, col.ClosestPoint(transform.position + Vector3.up), transform.rotation);
                Destroy(hitFX, 2f);

                spiderHealth -= .5f;
            }
        }
    }

    public void ColliderOn()
    {
        colAttack.enabled = true;
    }
}

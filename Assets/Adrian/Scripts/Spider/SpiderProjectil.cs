using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderProjectil : MonoBehaviour
{   
  
    float time;
    private Rigidbody myRigi;
    [SerializeField] float fuerzaDisparo;
    private Vector3 myinitialposition, ubicacionTarget;
  
    void Start()
    {
        
       GameObject target = GameObject.Find("Player");
        ubicacionTarget = target.transform.position;
        myRigi = GetComponent<Rigidbody>();   
        Vector3 dir = ubicacionTarget - transform.position;
        myRigi.AddForce(dir * fuerzaDisparo);
        Destroy(this.gameObject, 4f);
    }
   
   
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Player"))
        {
            Destroy(this.gameObject);
        }       
    }
   
}

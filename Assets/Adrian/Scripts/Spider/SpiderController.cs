using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SpiderController : MonoBehaviour
{
    [SerializeField] Transform homeTransform;
    [SerializeField] float moveDuration;
    [SerializeField] float wantStepAtDistance;
    [SerializeField] float distFromHome;
    [SerializeField] float stepOvershootFraction;

    public bool moving;  
    
    void Update()
    {
        if (moving) return; 

         distFromHome = Vector3.Distance(transform.position, homeTransform.position);

        if(distFromHome > wantStepAtDistance)
        {   
            StartCoroutine(MovetToHome());
        }
    }
   public void TryMove()
    {
        if (moving) return;

        distFromHome = Vector3.Distance(transform.position, homeTransform.position);

        if (distFromHome > wantStepAtDistance)
        {
            StartCoroutine(MovetToHome());
        }
    }
    IEnumerator MovetToHome()
    {
      
        moving = true;

        Vector3 startPoint = transform.position;
        Quaternion startRot = transform.rotation;

        Quaternion endRot = homeTransform.rotation;

        Vector3 towardHome = (homeTransform.position - transform.position);

        float overshootDistance = wantStepAtDistance * stepOvershootFraction;

        Vector3 overshootVector = towardHome * overshootDistance;
        overshootVector = Vector3.ProjectOnPlane(overshootVector, -Vector3.forward);

        Vector3 endPoint = homeTransform.position + overshootVector;
        
        Vector3 centerPoint = (startPoint + endPoint) / 2;
        centerPoint += -homeTransform.up * Vector3.Distance(startPoint, endPoint);

        
        float timeElapsed = 0;

        do
        {
           
            timeElapsed += Time.deltaTime;         

            float normalizedTime = timeElapsed / moveDuration;
            
            transform.position = Vector3.Lerp(Vector3.Lerp(startPoint,centerPoint,normalizedTime),Vector3.Lerp(centerPoint, endPoint, normalizedTime),normalizedTime);
                       
            transform.rotation = Quaternion.Slerp(startRot, endRot, normalizedTime);

            yield return null;

        }
        while (timeElapsed < moveDuration);

        moving = false;
    }
    
}

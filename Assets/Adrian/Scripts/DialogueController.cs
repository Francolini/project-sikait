using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


public class DialogueController : MonoBehaviour
{
    public TextMeshProUGUI textDisplay;
    public string[] sentences;
    public GameObject controlImage;
    private int index, indexImages;
    public float typingSpeed;
    public bool activeChat;
    private bool actived;
    [SerializeField] RawImage Background, triangle;
   
    public GameObject triggerToDestroy;

    
    // Start is called before the first frame update
    void Start()
    {
        
        indexImages = 0;       
        Background.enabled = false;
        triangle.enabled = false;
        actived = false;     
        
    }

    private void DestroyTrigger()
    {
        if (triggerToDestroy != null)
        {
            Destroy(triggerToDestroy);
        }
    }

    IEnumerator Type()
    {
        activeChat = true;
       
        Background.enabled = true;
        triangle.enabled = true;

        foreach (char letter in sentences[index].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
        yield return new WaitForSeconds(4.5f);

        if (index < sentences.Length -1)
        {
            NextSentence();
        } 
        else
        {
            activeChat = false;
            textDisplay.text = "";
            textDisplay.enabled = false;
            Background.enabled = false;
            triangle.enabled = false;
            controlImage.SetActive(false);
        }                      
    }
    public void NextSentence()
    {
        if (activeChat)
        {                     
                index++;
                textDisplay.text = "";
                StartCoroutine(Type());                                    
        }     
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player" && actived == false)
        {
            DestroyTrigger();
            StartCoroutine(Type());           

            textDisplay.enabled = true;
           
            actived = true;
            controlImage.SetActive(true);            
        }
    }

    private void OnDestroy()
    {
        textDisplay.text = "";
        controlImage.SetActive(false);
        StopCoroutine(Type());
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralWalk : MonoBehaviour
{
    [SerializeField] LayerMask groundMask;
    [SerializeField] Transform body = default;
    [SerializeField] ProceduralWalk otherFoot = default;

    [SerializeField] float speed = 1;
    [SerializeField] float stepHeight = 1;
    [SerializeField] float stepLenght = 4;
    [SerializeField] float stepDistance = 4;

    [SerializeField] Vector3 footOffset = default;
    float footSpacing;
    Vector3 oldPosition, currentPosition, newPosition;
    Vector3 oldNormal, currentNormal, newNormal;
    float lerp;

    float timer, timeStep;


    // Start is called before the first frame update
    void Start()
    {
        footSpacing = transform.localPosition.x;
        currentPosition = newPosition = oldPosition = transform.position;
        currentNormal = newNormal = oldNormal = transform.up;
        lerp = 1;
        timeStep = 0.05f;
        timer = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        transform.position = currentPosition;
        transform.up = currentNormal;
        RaycastHit info;
        Ray ray = new Ray(body.position + (body.right * footSpacing), Vector3.down);

        if (Physics.Raycast(ray, out info, 10, groundMask))
        {
            if (Vector3.Distance(newPosition, info.point) > stepDistance && !otherFoot.isMoving() && lerp >= 1)
            {
                lerp = 0;
                int direction = body.InverseTransformPoint(info.point).z > body.InverseTransformPoint(newPosition).z ? 1 : -1;
                if(timer >= timeStep)
                {
                    newPosition = info.point + (body.forward * stepLenght * direction) / 50 + footOffset;
                    newNormal = info.normal;
                    timer = 0f;
                }
              
            }

            if(lerp < 1)
            {
                Vector3 tempPosition = Vector3.Lerp(oldPosition, newPosition, lerp);
                tempPosition.y += Mathf.Sin(lerp * Mathf.PI) * stepHeight;
                currentPosition = tempPosition;
                currentNormal = Vector3.Lerp(oldNormal, newNormal, lerp);
                lerp += Time.deltaTime * speed;                
            }
            else
            {
                oldPosition = newPosition;
                oldNormal = newNormal;
            }       
                     
   
        } 
           
      
           
    }
    private void OnDrawGizmos()
    {

        Gizmos.color = Color.red;
        Gizmos.DrawSphere(newPosition, .1f);
    }
    public bool isMoving()
    {
        return lerp < 1; 
    }
   
           
}

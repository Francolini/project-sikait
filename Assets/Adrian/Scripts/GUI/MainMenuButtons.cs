using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuButtons : MonoBehaviour
{
    [SerializeField] GameObject backgroundImage, cinematicaGO, musicaGO;
    public Button playButton;

    [SerializeField] bool auxCoroutina;
  

    
    private void Awake()
    {
       
    }
    private void Start()
    {
     backgroundImage.SetActive(true);
        playButton.Select();
        auxCoroutina = true;
        
    }
   
    public void ButtonPlay()
    {
        if (auxCoroutina)
        {
            cinematicaGO.SetActive(true);
            backgroundImage.SetActive(false);
            StartCoroutine(timeWaitStart());
            musicaGO.SetActive(false);
        }     
        
    }

    public void ButtonExit()
    {
        Application.Quit();
    }

    IEnumerator timeWaitStart()
    {
        auxCoroutina = false;
        yield return new WaitForSeconds(99f);
        SceneManager.LoadScene("Cristian 1");
    }

}

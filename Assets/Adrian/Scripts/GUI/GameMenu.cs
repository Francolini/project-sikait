using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour
{
    [SerializeField] private GameObject  optionsMenu, dieMenu, mainMenuPause, controlMandoImage, controlKeyboardImage, buttonReturn, menuPause, sliders, buttonResumeGO, rewindBossZonePrefab, rewindBossZone = null, sliderBGHealthBoss, sliderHealthBoss, sliderDarkPower;

    private bool menuSettings;

    [SerializeField] private PlayerInput input;
    [SerializeField] private Button buttonControlGamepad, buttonControlKeyboard, resumeButton, backButton, retryButton;
    [SerializeField] private CheckpointsManager cm;
    [SerializeField] private EnemiesControllerSpawner ecs;
    public bool inPause = false;
    [SerializeField] private Transform bossRewindPoint;
    [SerializeField] private AnubisController anubis;

    public PlayerController pc;

    public MakePiecesFall makePiecesFal;

    private void Update()
    {
        if(!inPause)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (input.actions["Pause"].triggered && pc.enabled && !pc.dead)
        {
            pc.enabled = false;
            menuPause.SetActive(true);
            Time.timeScale = 0.00001f;
            sliders.SetActive(false);

            resumeButton.Select();

            inPause = true;
        }

        if (input.actions["Interact"].triggered && !pc.enabled && !pc.dead)
        {
            ButtonReturn();
        }
    }

    public void ButtonResume()
    {
        Cursor.lockState = CursorLockMode.Locked;
        inPause = false;
        backButton.Select();
        sliders.SetActive(true);
        Time.timeScale = 1;
        menuPause.SetActive(false);
        pc.enabled = true;
    }

    public void ButtonRestart()
    {
        SceneManager.LoadScene("Adrian");
    }
    
    public void ButtonRestartCheckpoint()
    {      
        rewindBossZone = Instantiate(rewindBossZonePrefab, bossRewindPoint.position, bossRewindPoint.rotation);
        rewindBossZone.name = "BossRewindZone";

        pc.dieMenuOpened = false;
        sliders.SetActive(true);
        ecs.SpawnEnemies();
        pc.gameObject.transform.position = cm.actualCheckpoint.transform.position;
        pc.RestartPlayer();
        dieMenu.SetActive(false);

        StartCoroutine("FallStoneBoss");       
        
    }

    public void ButtonSettings()
    {
        menuSettings = true;
        mainMenuPause.SetActive(false);
        optionsMenu.SetActive(true);
        buttonReturn.SetActive(true);
        buttonControlKeyboard.Select();
    }

    public void ButtonExit()
    {
        Application.Quit();
    }

    public void ButtonControlMando()
    {
        controlMandoImage.SetActive(true);
        optionsMenu.SetActive(false);
        menuSettings = false;
        backButton.Select();
    }

    public void ButtonControlKeyboard()
    {
        controlKeyboardImage.SetActive(true);
        optionsMenu.SetActive(false);
        menuSettings = false;
        backButton.Select();
    }

    public void ButtonReturn()
    {
        if (!menuSettings && !controlKeyboardImage.activeInHierarchy && !controlMandoImage.activeInHierarchy)
        {
            ButtonResume();
        }

        if (menuSettings)
        {
            menuSettings = false;
            mainMenuPause.SetActive(true);
            buttonReturn.SetActive(false);
            optionsMenu.SetActive(false);
            resumeButton.Select();
        }

        if(controlKeyboardImage.activeInHierarchy || controlMandoImage.activeInHierarchy)
        {
            menuSettings = true;
            optionsMenu.SetActive(true);

            if (controlKeyboardImage.activeInHierarchy)
            {
                buttonControlKeyboard.Select();
            }
            else
            {
                buttonControlGamepad.Select();
            }

            controlMandoImage.SetActive(false);
            controlKeyboardImage.SetActive(false); 
        }    
    }

    public void ActiveDieMenu()
    {
        rewindBossZone = GameObject.Find("BossRewindZone");
        Destroy(rewindBossZone);

        ecs.DestroyEnemies();

        inPause = true;
        dieMenu.SetActive(true);
        sliderDarkPower.SetActive(false);
        sliderHealthBoss.SetActive(false);
        sliders.SetActive(false);

        retryButton.Select();
    }

    private IEnumerator FallStoneBoss()
    {
        yield return new WaitForSeconds(.1f);
        
        makePiecesFal.MakePiecesFallAction();
    }
}

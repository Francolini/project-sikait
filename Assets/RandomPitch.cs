using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPitch : MonoBehaviour
{
    [SerializeField]
    private float min, max;
    private MusicController mc;
    // Start is called before the first frame update
    private void Start()
    {
        mc = GameObject.Find("MusicController").GetComponent<MusicController>();

        GetComponent<AudioSource>().pitch += Random.Range(min, max + .1f);
    }

    private void Update()
    {
        GetComponent<AudioSource>().volume = mc.sfxVolume;
    }
}
